{ config, pkgs, niximport, ... }:

with builtins;

# fun image gathered on internet. Hope that a backup exist somewhere
#TODO: use IPFS for backup
let
	mariusnur = niximport.mariusnur;
	
	imagemix = import ./imagemix.nix { inherit pkgs mariusnur; };
in
{
	home.file = {
		#"Images/gahoole/diggerdan.jpg".source = fetchurl {
		#	url = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/intermediary/f/c1dc01c7-3881-4ca5-b1d8-edf5019fa2bd/d9xf7fv-d772170a-4035-4f95-ac78-6597038cc75f.jpg/v1/fill/w_900,h_675,q_70,strp/digger_dan_by_projectowl_d9xf7fv-fullview.jpg";
		#	sha256 = "056jmmbrh59sm18bpjrp19jj0xpvrssfz9a9wcl8chn5mdqx1ca5";
		#};#TODO: move in imagemix

		#"Images/gahoole/projectowl-micaiah-revisit.jpg".source = fetchurl {
		#	url = "https://www.deviantart.com/download/792065663/dd3kprz-377e5a7e-3fe6-4503-96a2-ec86e7ebf3f3?token=8ecf727e9cbf525f04d9bb0211b309298702fe09&ts=1557734287";
		#	sha256 = "0qc9612icnmv4wmv35a3pr4qy2bhf9hk2xzawqwmb93brfcksniq";
		#	name = "micaiah.jpg";
		#};

		# also avalaible here : https://66.media.tumblr.com/d7c20c60b52277459dc9e9a1260c2c90/tumblr_oqu1tdVx351vvzvono1_1280.jpg
		"Images/futurama/otter.jpg".source = fetchurl {
			url = "https://geodedog.weebly.com/uploads/8/1/1/0/81108808/futurama_orig.jpg";
			sha256 = "0whhwr0i7gh1dbkp07yhhbgmryk2c71rma7yxw8w4dgw2bxqx4p2";
		};

		#"Images/pmd/gen8.png".source = fetchurl {
		#	url = "14a1ca5-fe9b-47bb-8bdf-805d2f24841f/dd182hy-2dc4ec30-9250-44c0-b9ac-2825f6b3d985.png/v1/fill/w_946,h_845,q_70,strp/pmd__but_gen_8_by_teeterglance_dd182hy-pre.jpg?token=eyJ0eXAi";
		#	sha256 = "0whhwr0i7gh1dbkp07yhhbgmryk2c71rma7yxw8w4dgw2bxqx3p2";
		#};

		"Images/md/altans.png".source = fetchurl {
			url = "https://i.redd.it/wuuorv3qy2g31.png";
			sha256 = "0zg1x2lkb2wbr4kvmnp7yixsi1l0cf9mjnis3bz949d18rbgj8x0";
		};

		"Images/TLOS/Dawn of the dragon is....jpg".source = imagemix.TLOS.DOTDIs;

		"Images/mlp/ShysShores.png".source = imagemix.mlp.shysShores;
		"Images/mlp/teapartanimal.jpg".source = imagemix.mlp.teapartanimal;
		"Images/mlp/hippogriffKingdom.jpg".source = imagemix.mlp.hippogriffKingdom;
	};
}
