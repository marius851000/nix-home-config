{ config, pkgs, niximport, ... }:

with builtins;

let
  images = niximport.images;
  
  wallpapers = {
    "mystery.jpg" = images.pokemon.mysterydungeon.largeFront;

    "deltarune.png" = images.deltarune.wallpaper_SiDniTheFox;

    "scp.jpg" = images.scp.scp76_wallpaper;

#    "pac-microuniverse.jpg" = images.peppercarrot.microuniverse;

#    "pac-episode29.jpg" = images.peppercarrot.dragon_e29;

    "haycgel_offer.jpg" = images.pokemon.mysterydungeon.haychel.offer;

    "mdthunkteam.png" = images.pokemon.mysterydungeon.mdThunknDunk;

#    "cynder.png" = images.TLOS.cynderWallpaper;

    "konqiReignited.png" = images.konqiReignitedStyle;

    "hoolemouse.jpg" = images.hoole.mouse;

#    "temple.jpg" = images.somatonic.temple;

    "thankyou.jpg" = images.mlp.thankyou;
    "sunnyalicorn.png" = images.mlp.sunnyalicorn;
    "izzy.png" = images.mlp.izzy;

    #"animated-loyalty.wepb" = images.mlp.loyaltyAnimated;

    "animated-red-forest.gif" = images.mlp.red-forest;
    "animated-red-forest-upscaled.webp" = images.mlp.red-forest-upscaled-webp;
    "animated-red-forest-upscaled.mp4" = images.mlp.red-forest-upscaled-mp4;

    # generated with dolphin on an european rom of the legend of guardians. Try to put this on IPFS (maybe)
    "gahoole_eagle.png" = ./wallpaper/tex1_852x480_m_ab9166e9679894b0_14.png;
    "gahoole_attack.png" = ./wallpaper/tex1_852x480_m_11df705035d16b4b_14.png;
    "gahoole_hagsom.png" = ./wallpaper/tex1_852x480_m_e54b58fb621334d0_14.png;
    # ".wallpaper/gahoole_building.png".source = ./wallpaper/tex1_852x480_m_05941294cd32be28_14.png;
    "gahoole_attack_2.png" = ./wallpaper/tex1_852x480_m_20441857b102ca5a_14.png;
    "gahoole_bat.png" = ./wallpaper/tex1_852x480_m_e4d6ee7e0cc60062_14.png;
    "gahoole_composed.png" = ./wallpaper/recomposed.png;
    "gahoole_singleowl.png" = ./wallpaper/tex1_852x480_m_1b3cdbf826001cdf_14.png;
    "gahoole_battle.png" = ./wallpaper/tex1_852x480_m_b9ad6100ebecaed7_14.png;
    "gahoole_hags2.png" = ./wallpaper/tex1_852x480_m_eb48a6b50565286e_14.png;
    "sunnybelle.jpg" = ./root/sunnybelle.jpg;

    "somethingfunny.jpg" = ./wallpaper/interesting.jpg;
  };

  #TODO: maybe upstream to home-manager
  wallpaper_folder = with pkgs; stdenv.mkDerivation {
    name = "wallpaper-config";

    dontUnpack = true;

    installPhase = lib.concatStringsSep "\n" ([
      "mkdir -p $out/share/wallpapers"
    ] ++ (lib.mapAttrsToList (k: v: "ln -s ${v} $out/share/wallpapers/${k}") wallpapers));
  }; #TODO: name them
in
{
  home.file = 
    pkgs.lib.mapAttrs' (name: value: pkgs.lib.nameValuePair ".wallpaper/${name}" {source = value;}) wallpapers;

  home.packages = [ wallpaper_folder ];
}
