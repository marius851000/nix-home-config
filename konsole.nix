{config, pkgs, ...}:

{
	home.file.".local/share/konsole/BreezeCustom.colorscheme".source = source/konsole/BreezeCustom.colorscheme;
	home.file.".local/share/konsole/Nix.profile".source = source/konsole/Nix.profile;
}
