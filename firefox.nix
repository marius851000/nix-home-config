{ config, pkgs, ... }:

with builtins;

let
	firefoxConfig = ".mozilla/firefox";
	firefoxHome = "${firefoxConfig}/marius";
	nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
		inherit pkgs;
	};
in
{
	home.file = {
		"${firefoxConfig}/profiles.ini".text = ''
		[General]
		StartWithLastProfile=1

		[Profile0]
		Name=marius
		IsRelative=1
		Path=marius
		Default=1
'';

		"${firefoxHome}/user.js".text = ''
	user_pref("browser.search.countryCode", "FR");
	user_pref("browser.search.region", "FR");
	user_pref("browser.startup.homepage", "https://duckduckgo.com");
	'';

	};

	#TODO : auto download extension

	nixpkgs.config.firefox.enablePlasmaBrowserIntegration = true;

	programs.firefox = {
		enable = true;
		package = pkgs.firefox.override {
			cfg.enablePlasmaBrowserIntegration = true;
		};
		#extensions = with nur.repos.rycee.firefox-addons; [
      	#	https-everywhere ublock-origin
    	#]; #TODO: no sha
		#TODO: dark reader
	};

}
