{ stdenv, lib, waifu2x-converter-cpp, ffmpeg-full }:

let
    make_6_number_string = number: lib.fixedWidthNumber 6 number;
in {
    input,
    name,
    frames_nb, #TODO: auto count using some method
    additional_args ? "",
    extension ? "webp"
}:

let
    splited = stdenv.mkDerivation {
        name = "splited-${name}";

        dontUnpack = true;

        buildPhase = ''
            mkdir out
            ${lib.makeBinPath [ffmpeg-full]}/ffmpeg -i ${input} out/%06d.png
        '';

        installPhase = ''
            mv out $out
        '';
    };

    frame_upscaled = id: stdenv.mkDerivation {
        name = "frame-${toString id}-${name}-upscaled.png";

        dontUnpack = true;

        buildPhase = ''
            ${lib.makeBinPath [waifu2x-converter-cpp]}/waifu2x-converter-cpp \
                -i ${splited}/${make_6_number_string id}.png \
                -o out.png ${additional_args}
        '';
        
        installPhase = ''
            mv out.png $out
        '';
    };

    joined_result = stdenv.mkDerivation {
        name = "joined-${name}";

        dontUnpack = true;

        buildPhase = lib.concatStringsSep "\n" ([
            "mkdir out"
        ] ++ (map (x: "ln -s ${frame_upscaled x} out/${make_6_number_string x}.png") (lib.range 1 frames_nb)));

        installPhase = "mv out $out";
    };
in
    stdenv.mkDerivation {
        name = "${name}-upscaled.${extension}";

        dontUnpack = true;

        nativeBuildInputs = [ ffmpeg-full ];

        buildPhase = ''
            ffmpeg -framerate 25 -pattern_type glob -i "${joined_result}/*.png" -loop 0 result.${extension}
        ''; #TODO: auto determine framerate from input file
        #TODO: maybe lossless is not a good idea

        installPhase = "cp result.${extension} $out";
    }