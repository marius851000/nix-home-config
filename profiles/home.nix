{
  use_private = true;

  hm = (
    niximport:
    { pkgs, lib, ... }:
    {
      imports = [
        (import ../game.nix)
        (import ../images.nix)
      ];

      programs.git.userEmail = "marius@mariusdavid.fr";

      home.packages = with pkgs; [
        # games
        lutris-free
        #minetest
        ndstool
        #steam
        xonotic
        # long compile time
        /*(dolphinEmuMaster.override { stdenv = (impureUseNativeOptimizations stdenv); })
          (subtitleeditor.overrideAttrs (oldAttrs: { buildInputs = oldAttrs.buildInputs ++ [ gst_all_1.gst-plugins-bad ]; }))*/
        /*(pkgs.wineWowPackages.full.override { wineRelease = "staging"; })*/
        pkgs.wineWowPackages.full
        # censored
        #      tor-browser-bundle-bin
        #nsteam-run

        /*funkinnur.friday-night-funkin
        funkinnur.smoke-em-out-struggle
        funkinnur.vs-impostor
        funkinnur.vs-whitty
        funkinnur.vs-myra
        funkinnur.softmod
        funkinnur.explorers_of_death*/
      ];
    }
  );

  nixos = { pkgs, lib, ... }: {
    networking.hostName = "lutta";
  };
}