{
  use_private = false;

  hm = (
    niximport:
    { pkgs, lib, ... }:
    {
      programs.git.userEmail = "DAVID24Ma@lycee-ndduroc.com";
    }
  );

  nixos = { pkgs, lib, ... }: {
    networking.hostName = "marius-nixos-school";
  }
}