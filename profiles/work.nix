{
  use_private = true;

  hm = (
    niximport:
    { pkgs, lib, ... }:

    {
      home.packages = with pkgs; [
        #nodejs-12_x
        #nodePackages.npm
        slack
      ];

      programs.git.userEmail = "m.david@bimedia.org";

    }
  );

  nixos = { pkgs, lib, ... }: {
    networking.hostName = "marius-nixos-bimedia";

    sound.enable = lib.mkForce false;
    hardware.pulseaudio.enable = lib.mkForce false;

    security.rtkit.enable = true;
    services.pipewire = {
      systemWide = false;
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };


    services.mongodb.enable = true;
    services.xserver.displayManager = {
      sddm.enable = lib.mkForce false;
      gdm.enable = lib.mkForce true;
    };
    virtualisation.virtualbox.host = {
      enable = lib.mkForce false;
    };
  };
}
