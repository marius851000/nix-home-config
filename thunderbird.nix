{ config, pkgs, ... }:

with builtins;

# thunderbird setting

let
	thunderbirdPath = ".thunderbird";
	thunderbirdConfigPath = "${thunderbirdPath}/marius";

	thunderblurrd_source = pkgs.fetchFromGitHub {
		owner = "manilarome";
		repo = "thunderblurred";
		rev = "2d577b5aa2d434ce40a1b0f542919212a8b5c1da";
		sha256 = "12c9qlbgnsnagmkg79q9shg536nd7ldibmv2008cyawyfwfh80ff";
	};
in
{
	home.file."${thunderbirdPath}/profiles.ini".text =
''[General]
StartWithLastProfile=1

[Profile0]
Name=marius
IsRelative=1
Path=marius
Default=1'';

#	home.file."${thunderbirdConfigPath}/Mail/Feeds/feeds.rdf".source = ./private/feeds.rdf;

	home.file."${thunderbirdConfigPath}/user.js".text = ''
		user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
		user_pref("layers.acceleration.force-enabled", true);
		user_pref("gfx.webrender.all", true);
		user_pref("gfx.webrender.enabled", true);
		user_pref("svg.context-properties.content.enabled", true);
	'';

	home.file.".thunderbird/XXXXXXX.default-release/chrome".source = thunderblurrd_source;

	home.packages = [
		pkgs.thunderbird
	];

	#TODO: use a module when availaible
}
