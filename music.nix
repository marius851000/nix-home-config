{ config, pkgs, ... }:

with pkgs;
with builtins;
with lib;

# music gathered on internet. Hope that a backup exist somewhere

let
	musicPath = "Musiques";
in
{

	# 6klop album Naskigo
	home.file = {
		"${musicPath}/Naskigo".source = fetchzip {
	    url = "https://archive.org/compress/MI235-6klop-Naskigo/formats=OGG%20VORBIS&file=/MI235-6klop-Naskigo.zip";
	    sha256 = "0rnp3lrks1zwf8y02qv27d4lmirvkalgkplp3bcz01la9iv8xrba";
			stripRoot = false;
	  };

		# Pokémon super mystery dungeon OST
#		"${musicPath}/PSMD".source = fetchzip {
#			url = "http://176.9.16.248/soundfiles/nintendo-3ds-3sf/pokemon-super-mystery-dungeon/yezuhiok/Pokemon%20Super%20Mystery%20Dungeon%20%28MP3%29.zophar.zip";
#			sha256 = "1qp4s23f8v8mx0qaid3550zc44gch1y09fvz744f7j1f27h8nwv5";
#			stripRoot = false;
#		};

		# Pokémon mystery dungeon : explorer of sky OST
		# TODO. Maybe get fun with ipfs here, or extract the soundtrack directely from the game
	};

}
