{ allowUnfree = true;
  allowBroken = true;
  allowUnsupportedSystem = true;
  permittedInsecurePackages = [
         "openssl-1.0.2u"
         "ffmpeg-2.8.17"
         "libav-11.12" #TODO:
         "spidermonkey-38.8.0"
       ];
 }

