{ pkgs, niximport, ... }:

with builtins;

let
  sppBackupDir = "~/sspcomics";
  ghidra-extension = with niximport.mariusnur.ghidraPackage; [ ghidra-gekko-broadway-lang ];

  ghidra-with-extension = pkgs.ghidra-bin.overrideAttrs (
    oldAttrs: {
      preFixup = pkgs.lib.concatStringsSep "\n" (
        map (x: "cp -r ${x}/ghidra $out/lib/ghidra/") ghidra-extension
      );
    }
  );

  script_generate_user = pkgs.writeShellScriptBin "generate_db_user" ''
    echo creating database for user "$1"
    sudo mysql -u root -e "CREATE DATABASE $1"
    sudo mysql -u root -e "CREATE USER $1@localhost IDENTIFIED BY '$1'"
    sudo mysql -u root -e "GRANT ALL PRIVILEGES ON $1.* TO $1@localhost"
    sudo mysql -u root -e "FLUSH PRIVILEGES"
  '';

  working_akonadi_version = import
    (pkgs.fetchFromGitHub {
      owner = "NixOS";
      repo = "nixpkgs";
      rev = "bbd05c5a61858f85091f32313d5b1a097fe0abc1";
      sha256 = "sha256-1mFCjBI6vBU5cPIdSY/RakJH9roLntGRAiCOTUjSBWo=";
    })
    { system = pkgs.system; };
in
{
  home.stateVersion = "22.05";
  programs = {
    home-manager.enable = true; # do not remove this
    git = {
      package = pkgs.gitAndTools.gitFull;
      enable = true;
      delta.enable = true;
      lfs.enable = true;
      userName = "marius david";
      extraConfig = {
        color = {
          status = "auto";
          diff = "auto";
          branch = "auto";
        };
      };
    };
  };

  home.sessionVariables = {
    LANGUAGE = "fr_FR";
    AMD_DEBUG = "nodmacopyimage";
  };

  imports = [
    ./thunderbird.nix # thunderbird mail client
    (import ./fish.nix) # fish shell
    (import ./plasma.nix) # plasma DE and KWin WM
    (import ./wallpaper.nix) # wallpaper
    ./music.nix # My music
    ./firefox.nix # firefox
    (import ./newsboat.nix) # software I update
    ./discord.nix # discord and glasscord
    ./konsole.nix
    (import ./mpv.nix)
    (import ./vscode.nix)
    (import ./rescrobbled.nix)
    ./traduction_abusive.nix
  ] ++ (
    let
      hm_profile = niximport.profileutil.get_or_default niximport.profile "hm" null;
    in
    (
      if (hm_profile != null) then [
        (hm_profile niximport)
      ] else [ ]
    )
  );

  # TODO: auto configure qt and gtk theme
  # TODO: kdeconnect
  # TODO: firefox
  # TODO: play with ipfs

  home.packages = with pkgs; with libsForQt5;  [
    /*(fuse-7z-ng.override {
      p7zip = p7zip.override { enableUnfree = true; };
    })*/
    (python3.withPackages (ps: with ps; [ wget requests lxml sqlalchemy pyqt5 redis docopt numpy websockets pyaudio pillow ]))
    projectm
    kubo
    gwenview
    inkscape
    lolcat
    desmume
    clang
    elisa
    lua
    bat
    filezilla
    rclone
    sqlitebrowser
    wget
    gource
    nmap
    httrack
    jq
    nixpkgs-review
    giac-with-xcas
    gallery-dl
    aria2
    megatools
    mdbook
    eva
    lsof
    qbittorrent
    scrot
    gamecube-tools
    compsize
    bees
    neofetch
    trash-cli
    masscan
    flamegraph
    #ghidra-with-extension
    #dbeaver
    youtube-dl
    blender
    ctrtool
    klavaro
    mupdf
    radare2
    rustup
    xorg.xmodmap
    wxhexeditor
    gimp
    kdenlive
    imagemagick
    ffmpeg-full
    vlc
    libreoffice
    josm
    uget
    xscreensaver
    nixpkgs-fmt
    niximport.mariusnur.mic_over_mumble
    script_generate_user #TODO: move in mariusnur

    #TODO: cleanup. There is way too much stuff here
    # kde software, and their buggy depency
    akregator
    ark
    baloo
    calendarsupport
    dolphin
    dolphin-plugins
    eventviews
    ffmpegthumbs
    filelight
    gwenview
    incidenceeditor
    k3b
    kaddressbook
    kate
    kcachegrind
    kcalc
    kcalutils
    kcalendarcore
    kcolorchooser
    kcontacts
    kdav
    kparts
    kdegraphics-mobipocket
    kdegraphics-thumbnailers
    kdenetwork-filesharing
    libsForQt5.kdepim-runtime
    libsForQt5.kdepim-addons
    kdf
    kgpg
    khelpcenter
    libsForQt5.kholidays
    libsForQt5.kidentitymanagement
    libsForQt5.ksmtp
    kig
    libsForQt5.kimap
    libsForQt5.kio-extras
    libsForQt5.kldap
    kleopatra
    kmail
    libsForQt5.kmail-account-wizard
    libsForQt5.kmailtransport
    libsForQt5.kmbox
    libsForQt5.kmime
    kmix
    kolourpaint
    libsForQt5.kompare
    konsole
    kontact
    kdevelop
    qmake
    korganizer
    libsForQt5.kpimtextedit
    kqtquickcharts
    krfb
    ktnef
    kwalletmanager
    libgravatar
    libkcddb
    libkdcraw
    libkdepim
    libkexiv2
    libkipi
    libkleo
    libkomparediff2
    libksieve
    mailcommon
    mailimporter
    mbox-importer
    messagelib
    okteta
    okular
    pimcommon
    pim-data-exporter
    pim-sieve-editor
    print-manager
    spectacle
    syndication
    plasma5Packages.kdeconnect-kde
    konversation
    #TODO: doesn't work yet
    libsForQt5.akonadi-mime
    libsForQt5.akonadi-calendar
    libsForQt5.akonadi-search
    libsForQt5.akonadi-notes
    libsForQt5.akonadi-contacts
    libsForQt5.akonadi-import-wizard
    libsForQt5.akonadi
    kmymoney
    amarok


    niximport.ricenur.ricePkgs.plasmoids.smart_video_player
  ]
  ++ (
    if (niximport.private.factorio != null) then [
      (
        factorio.override {
          username = "marius851000";
          token = niximport.private.factorio.token;
        }
      )
    ] else [ ]
  );

  programs.obs-studio = {
    enable = true;
    plugins = [ ];
  };

  services.xscreensaver = {
    enable = true;
  };

  services.syncthing.enable = true;

  home.file.".local/share/templates".source = ./templates;

  home.file.".config/gallery-dl/config.json".text = builtins.toJSON {
    extractor = {
      sspcomics = {
        filename = "{page} - {name}.{extension}";
        directory = [ "{serie}" ];
        base-directory = "${sppBackupDir}";
        archive = "${sppBackupDir}/archive.sqlite3";
      };
      deviantart = {
        refresh-token = "cache";
        metadata = true;
        original = true;
      } // (
        if ("deviantart" ? niximport.private) then {
          #client-id = private.deviantart.clientid;
          client-secret = niximport.private.deviantart.clientsecret;
        } else { }
      );
    };
    output = {
      mode = "color";
      progress = "%current/%total - %url";
    };
    postprocessors = [
      { name = "metadata"; }
    ];
  };


  # school backup


  systemd.user = {
    timers = {
      /*backup_often_bis = {
        Unit = { Description = "sync important document"; };

        Timer = {
        OnCalendar = "*-*-* *:00:00";
        Unit = "backup_often.service";
        };

        Install = { WantedBy = [ "timers.target" ]; };
        };*/
    };

    services = {
      /*backup_often_bis = {
        Unit = { Description = "sync important document"; };

        Service = {
        Type = "oneshot";
        ExecStart = "${pkgs.rclone}/bin/rclone sync -P /home/marius/Sync/cours2021-2022/ \"onedrive universite:cours2021-2022-backup\"";
        };
        };*/

      # based on https://www.guyrutenberg.com/2021/06/25/autostart-rclone-mount-using-systemd/
      mount_onedrive = { #TODO: only TUF #TODO: make same for work
        Unit = {
          Description = "mount the onedrive from school on the computer";
        };

        Service = {
          Type = "simple";
          ExecStart = "${pkgs.rclone}/bin/rclone mount --vfs-cache-mode full \"onedrive universite:\" %h/OneDrive --verbose";
          ExecStop = "${pkgs.fuse}/bin/fusermount -Zu %h/OneDrive";

          Restart = "on-failure";
          RestartSec = 45;
        };

        Install = {
          WantedBy = [ "default.target" ];
        };
      };
    };
  };

  #TODO: rather include ytcc in home-manager
  home.file.".config/ytcc/ytcc.conf".text = ''
    [YTCC]
    downloaddir = ~/Videos
    alphabet = aetuowcb
    [youtube-dl]
    loglevel = verbose
    ratelimit = 1000000
    retries = 4
    subtitles = fr,en
  '';
}
