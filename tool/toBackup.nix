# generate a folder containing unstable online content
{pkgs ? import <nixpkgs> {}}:

with builtins;

let
	getAll = x: name:
		let
			commandToRun = pkgs.lib.concatStringsSep "\n" (["mkdir $out"] ++ ((
					pkgs.lib.mapAttrsToList
						(n: v: if (builtins.isAttrs v) then
							"ln -s ${getAll v n} $out/${n}"
						else
							"ln -s ${v} $out/${n}"
					) x)))
			;
			fileToRun = builtins.toFile "builder.sh" ''
				fish -c "$commandToRun"
			'';
		in
			builtins.derivation {
				inherit commandToRun;
				name = "root";
				PATH = "${pkgs.coreutils}/bin:${pkgs.fish}/bin";
				builder = "${pkgs.fish}/bin/fish";
				system = pkgs.system;
				args = [fileToRun];
			};
in
getAll (import ../imagemix.nix {}) "root"
#getAll {} "root"
