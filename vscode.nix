{ pkgs, config, niximport, ... }:

let
  extension = source : pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = source;
  };

  #TODO: upstream, auto-update
  php-debug = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "php-debug";
      publisher = "xdebug";
      version = "1.25.0"; #TODO: auto patch path to php if php not in path
      sha256 = "sha256-jNkIlzVZO9IUeqMlqFReQSKjkh3WzBBi601XtEGbJ1M=";
    };
  };
  gitlive = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "teamhub";
      publisher = "TeamHub";
      version = "12.0.0";
      sha256 = "tUGEDUWWF28Z4Z1iCWzPJi0UrdbTae5E9seByos37Hw=";
    };
  };
  js-debug-nightly = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "js-debug-nightly";
      publisher = "ms-vscode";
      version = "2021.3.2417";
      sha256 = "dLWHZBTJ6Bx+MdRZIRNQ9qHC2CNOPnPM1DG1elHmdy0=";
    };
  };
  pokemon-code = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "pokemon-code";
      publisher = "mattkremer";
      version = "0.0.4";
      sha256 = "/C5F768mhKWSkbSLAgMlxkAh9l3lvBdXYhegHldYtAk=";
    };
  };
  live-html-previewer = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "live-html-previewer";
      publisher = "hdg";
      version = "0.3.0";
      sha256 = "";
    };
  };
  zscript = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "gzdoom-zscript";
      publisher = "kaptainmicila";
      version = "1.6.1";
      sha256 = "sha256-sO8NCSHDyPpkGvIJi2UWzZERhoRFCm4GT640LEsTz+0=";
    };
  };
  rainbow-csv = pkgs.vscode-utils.buildVscodeMarketplaceExtension {
    mktplcRef = {
      name = "rainbow-csv";
      publisher = "mechatroner";
      version = "1.8.1";
      sha256 = "sha256-Q4iIlRQFjM5/V7oY0BsYKmpRGJdnlVqFKyPaqBFaUs0=";
    };
  };
  composer = extension {
    name = "composer";
    publisher = "ikappas";
    version = "0.7.1";
    sha256 = "sha256-wjFtiUZIoXpxssxPFF9d0P+tpUAWUq16NUDNdCR2f1Q=";
  };
  laravel-blade = extension {
    name = "laravel-blade";
    publisher = "onecentlin";
    version = "1.25.0";
    sha256 = "sha256-5kojXSBF/+bLar1rgh+Df6v76qfnqY2u1SyuaVi4KCM=";
  };
  laravel5-snippets = extension {
    name = "laravel5-snippets";
    publisher = "onecentlin";
    version = "1.12.0";
    sha256 = "sha256-Z0dvgxuISp+KNmF4crIGYKz+9nr7bsZvDIThYvkLBbk=";
  };
  laravel-artisan = extension {
    name = "laravel-artisan";
    publisher = "ryannaddy";
    version = "0.0.27";
    sha256 = "sha256-4pH3cBijsqOhan/edfpeHTnRhhFmr5ie3wHBDu3KCZc=";
  };
  laravel-extra-intelissence = extension {
    name = "laravel-extra-intellisense";
    publisher = "amiralizadeh9480";
    version = "0.6.1";
    sha256 = "sha256-RN47EZRDYBKTUAkzQICKLdUfE7dixEDz4mGXylE0xkE=";
  };
  laravel-goto-view = extension {
    name = "laravel-goto-view";
    publisher = "codingyu";
    version = "1.3.4";
    sha256 = "sha256-7LdDQvZkQaEDpNyaaoOqpJBl75wkZYLyrbhq2hvJCl8=";
  };
  laravel-goto-controller = extension {
    name = "laravel-goto-controller";
    publisher = "stef-k";
    version = "0.0.12";
    sha256 = "sha256-6n6iK5WtWPE5kfKcKGe9muMgSrINfHVcQm/e/zyjqYY=";
  };
  dotenv = extension {
    name = "dotenv";
    publisher = "mikestead";
    version = "1.0.1";
    sha256 = "sha256-dieCzNOIcZiTGu4Mv5zYlG7jLhaEsJR05qbzzzQ7RWc=";
  };
  lorem-ipsum = extension {
    name = "lorem-ipsum";
    publisher = "Tyriar";
    version= "1.2.0";
    sha256 = "sha256-Bb3iopO8j0rfxeKSPEhQtkPxNUQr7J7c159HlK2BOYI=";
  };
  php-constructor = extension {
    name = "php-constructor";
    publisher = "MehediDracula";
    version = "0.1.2";
    sha256 = "sha256-fjnyEoDsVLylU/PvmgHT/E/eoY+HS0yHN/KJXidGef8=";
  };
  php-namespace-resolver = extension {
    name = "php-namespace-resolver";
    publisher = "MehediDracula";
    version = "1.1.8";
    sha256 = "sha256-lbwHstKL1cRsBKheMhJLJAEL2cywXDSlqhre83NS/cg=";
  };
  php-add-property = extension {
    name = "php-add-property";
    publisher = "kotfire";
    version = "1.1.0";
    sha256 = "sha256-/fLsDCBNvUhix0XBgNdwR4ehzNPygjLV+PDLKwWoaIw=";
  };
  phpunit-snippets = extension {
    name = "phpunit-snippets";
    publisher = "onecentlin";
    version = "1.1.0";
    sha256 = "sha256-OKmOuZ52wvbmy0c2rI2a1bEOjPNX9a+Z/vZeexslQeQ=";
  };
  vscode-phpunit = extension {
    name = "vscode-phpunit";
    publisher = "recca0120";
    version = "2.0.76";
    sha256 = "sha256-XFH1L+M58c8JjTmGt71yyHFjna5EzUWaEiTKCR8QxXU=";
  };
  rust = extension {
    name = "rust";
    publisher = "rust-lang";
    version = "0.7.8";
    sha256 = "sha256-Y33agSNMVmaVCQdYd5mzwjiK5JTZTtzTkmSGTQrSNg0=";
  };
  hxcpp-debugger = extension {
    name = "hxcpp-debugger";
    publisher = "vshaxe";
    version = "1.2.4";
    sha256 = "sha256-Jllwf32tJast1vNPQa2DmzZDzwCtyEJkb4YehAMQaEs=";
  };
  haxe = extension {
    name = "vshaxe";
    publisher = "nadako";
    version = "2.23.2";
    sha256 = "sha256-qb0iZKp8TBQ3iJWy2/hEtPcL/FvazFLi21hJlDYCmc0=";
  };
  lime = extension {
    name = "lime-vscode-extension";
    publisher = "openfl";
    version = "1.4.3";
    sha256 = "sha256-N7xhRHNsOKy4XaycInBWL4847JAq525Krm1dq6BvHLo=";
  };
  discord_presence = extension {
    name = "discord-vscode";
    publisher = "icrawl";
    version = "5.7.0";
    sha256 = "sha256-pN28MBf+pjEqg3jTTIuS43FcbzPSoIRbk+oT7CQbJ+g=";
  };
  volar = extension {
    name = "volar";
    publisher = "johnsoncodehk";
    version = "0.29.8";
    sha256 = "sha256-/IiIDMJuGsc3N90CsTA/FFb8lf2rwmH6fYGlKnnVH+s=";
  };
  qml = extension {
    name = "QML";
    publisher = "bbenoist";
    version = "1.0.0";
    sha256 = "sha256-tphnVlD5LA6Au+WDrLZkAxnMJeTCd3UTyTN1Jelditk=";
  };

  tabby = extension {
    name = "vscode-tabby";
    publisher = "TabbyML";
    version = "1.4.0";
    sha256 = "sha256-WNxZh57v79qdku1OSkqHnMhubnrBUiv9fpZntI9jW3U=";
  };
  
  /*org-vscode = pkgs.vscode-utils.buildVscodeExtension {
    name = "org-vscode";
    src = pkgs.fetchFromGitHub {
      owner = "vscode-org-mode";
      repo = "vscode-org-mode";
      rev = "98f123a97eca8d0cc1d9e3f313dc2a8be8f25d46";
      sha256 = "sha256-pNN2BpQcWja/HtENzwWiAGNB/RE7ntkiwlPHOmGRs1U=";
    };
    vscodeExtUniqueId = "vscode-org-mode.vscode-org-mode";
  };*/

  pkgs_good_rust_analyzer = import (pkgs.fetchFromGitHub {
    owner = "NixOS";
    repo = "nixpkgs";
    rev = "741277f5b8d85119dd9f4cc41cdf340b7e8f42c2";
    sha256 = "sha256-Zdzxo6xLN56Zav5Ix5b1E8izd+8Ga0L1T85p5CU9+Qc=";
  }) { system = "x86_64-linux"; };

  vspatched = (pkgs.vscode-with-extensions.override {
    vscode = pkgs.vscodium.overrideAttrs (old: {
      postInstall = "cat ${./custom_vscode_css.css} >> $out/lib/vscode/resources/app/out/vs/workbench/workbench.desktop.main.css";
    }); #TODO: if only there were an easy way to have layered settings... Harder with a programming tools than plasma (it's actually not really complex there).
    vscodeExtensions = with pkgs.vscode-extensions; [
      #org-vscode
      jnoortheen.nix-ide
      matklad.rust-analyzer
      #rust
      jock.svg
      skyapps.fish-vscode
      a5huynh.vscode-ron
      gruntfuggly.todo-tree
      #TODO php debug, intelissence and package into nixpkgs
      php-debug
      #gitlive
      ms-dotnettools.csharp
 #     js-debug-nightly
      zscript
  #    rainbow-csv
      composer
      laravel-blade
  #    laravel5-snippets
      laravel-artisan
      laravel-extra-intelissence
      laravel-goto-view
#      laravel-goto-controller
      dotenv
   #   lorem-ipsum
      /*php-constructor
      php-namespace-resolver
      php-add-property
      phpunit-snippets
      vscode-phpunit*/
      vadimcn.vscode-lldb
      octref.vetur
      hxcpp-debugger
      haxe
      WakaTime.vscode-wakatime
      lime
      discord_presence
      #volar
      dbaeumer.vscode-eslint
      ms-python.python
      qml
      editorconfig.editorconfig
      jnoortheen.nix-ide
      denoland.vscode-deno
      tabby
    ];
  });

  mkOutOfStoreSymlink = config.lib.file.mkOutOfStoreSymlink;
in
{
  #TODO: de-hardcode this path (the /home/marius/...)
  #home.file.".config/Codium/User/settings.json".source = mkOutOfStoreSymlink "/home/marius/.config/nixpkgs/symlink/vscode/settings.json";
  home.packages = [
    vspatched
    #pkgs.php80Packages.php-cs-fixer
    #pkgs.php80Packages.composer
    pkgs.nil
    /*(
      pkgs.php.withExtensions (
        { enabled, all }: with all; [
          pdo
          pdo_mysql
          xdebug
          dom
          #json
          filter
          iconv
          openssl
          mbstring
          simplexml
          curl
          filter
          session
          tokenizer
        ]
      )
    )*/
  ];
}
