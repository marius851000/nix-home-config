rec {
  get_or_default = profile: name: default:
    if profile == null then
      default
    else if (builtins.hasAttr name profile) then
      profile."${name}"
    else
      default
  ;

  get_in_list_or_empty = profile: name: let
    gotten = get_or_default profile name null;
  in
    if (gotten == null) then
      []
    else
      [gotten]
  ;
}
