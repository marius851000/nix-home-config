import subprocess
import uuid

directory_to_mirror = [ "/home/marius/.config/nixpkgs", "/home/marius/cours2019"]

mirror_file = open("/home/marius/mirrored.txt","w")

#TODO: use kwallet manager
f = open("./private/password.txt")
password = f.read().split("\n")[0]
f.close()

assert len(password) > 6

for directory in directory_to_mirror:
    temp_file = "/tmp/"+str(uuid.uuid4())
    subprocess.check_call(["tar", "-cf", temp_file+".tar.xz", directory])
    subprocess.check_call(["openssl","enc","-in",temp_file+".tar.xz","-out",temp_file+".tar.xz.enc","-e","-aes-256-cbc","-pass","pass:"+password])
    address = subprocess.check_output(["ipfs","add","-Qr","--pin=false",temp_file+".tar.xz.enc"])
    subprocess.check_call(["ssh","raspi","ipfs","pin","add",address])
    mirror_file.write(directory+"    "+str(address)+"\n")

mirror_file.close()
