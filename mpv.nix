{ config, pkgs, niximport, ... }:

{
  #from: https://github.com/LunNova/nixos-configs/tree/150784baab2c36d8b02de22b1cecbcc33fdf8fb2/users/lun/media
  home.packages = [
    (pkgs.wrapMpv (
      pkgs.mpv-unwrapped.override {
        vapoursynthSupport = true;
      })
      {
        scripts = [
          #mpvScripts.thumbnail
          #mpvScripts.youtube-quality
        ];
      }
    )
  ];

  home.file.".config/mpv/motioninterpolation.py".source = pkgs.substituteAll {
    src = ./source/mpv/motioninterpolation.py;
    mvtoolslib = "${pkgs.vapoursynth-mvtools}/lib/vapoursynth/";
  };

  home.file.".config/mpv/svp.py".source = pkgs.substituteAll {
    src = ./source/mpv/svp.py;
    svpflow = "${pkgs.callPackage source/mpv/svp.nix {}}/lib/";
    mvtoolslib = "${pkgs.vapoursynth-mvtools}/lib/vapoursynth/";
  };
}