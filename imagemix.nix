{ pkgs ? import <nixpkgs> { }, mariusnur }:

with builtins;

let
  fetchgalleryDL = mariusnur.lib.fetchgalleryDL;
  fetchurl = pkgs.fetchurl;
  upscale_video = mariusnur.lib.upscale_video;
in
{
  pokemon = {
    mysterydungeon = {
      largeFront = fetchurl {
        url = "https://i.imgur.com/4ZgjXQF.jpg";
        sha256 = "18p3y07q196vfla46hl7cb8xd6337z3693rqbhl1m3a2gylxdhgw";
      };
      haychel = {
        offer = (fetchgalleryDL {
          url = "https://d.facdn.net/art/haychel/1437213245/1437212954.haychel_little_shewolf.jpg";
          sha256 = "12di551lwxhq22sls44w3pn813q2v1kf724i0p9grl4yw66wavrf";
        }).outPath;
      };
      mdThunknDunk = fetchurl {
        url = "https://i.redd.it/2cf72hgwtuu41.png";
        name = "mdthunkndunk.png";
        sha256 = "1r8dm9lj84hqkxh7a00i8dw56z0wnsr2k7w3pippc0is0vc1sddy";
      }; #https://www.reddit.com/r/MysteryDungeon/comments/g7jl5p/here_is_the_much_requested_background_without_the/
    };
    reshiram = fetchurl {
      url = "https://api-da.wixmp.com/_api/download/file?downloadToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImV4cCI6MTU1NzczMzM4MSwiaWF0IjoxNTU3NzMyNzcxLCJqdGkiOiI1Y2Q5MWRhZDI1ZDIzIiwib2JqIjpudWxsLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdLCJwYXlsb2FkIjp7InBhdGgiOiJcL2ZcL2RjMjQwZWI4LTA4OWItNDg3NC1iZDM1LTRhODUxYzE0NmNlNFwvZGN2ZTlnOS05NGViNzg1MC00YWQxLTQ2NjQtODBhMS0yYWY1NmM1ZmMzZWQucG5nIiwiYXR0YWNobWVudCI6eyJmaWxlbmFtZSI6InJlc2hpcmFtX2J5X3I4YV9jcmVhdGlvbnNfZGN2ZTlnOS5wbmcifX19.WV8kbfyOCU65DCaGzhcHvnbanZ5uCf0FvE21WUath2g";
      name = "reshiram-wallpaper.png";
      sha256 = "1b4xyxchjj332hq5yfapwdmaqmmsqdnyshxjy14322gmyr0hlbhd";
    };
  };
  hoole = {
    mouse = (fetchgalleryDL {
      url = "https://luis_belerique.artstation.com/projects/JldQEm";
      sha256 = "sha256-6l+buFOw+XMXXYCM6y/V7mCelsFOlaVXoZHrQeP0IHc=";
    }).outPath;
  };
  deltarune = {
    wallpaper_SiDniTheFox = fetchurl {
      # https://www.furaffinity.net/view/29246568/
      url = "https://d.facdn.net/art/sidnithefox/1541170082/1541169995.sidnithefox_fa-deltarune-wallpaper.png";
      sha256 = "0q4qxcf2gjspkkwldp4wdsi25minw4vb6p1fkxg1zcjd1vznfvhj";
    };
  };
  scp = {
    scp76_wallpaper = fetchurl {
      # https://www.reddit.com/r/SCP/comments/avmnef/didnt_bright_tell_us_to_keep_eye_contact_with_it/
      url = "https://i.redd.it/2nm6mra3h8j21.jpg";
      sha256 = "16cr3ydky6m0cnyp6yizfql5nqnb6vbl1cm07yymwi38nqk6smcb";
    };
  };
  peppercarrot = {
    microuniverse = fetchurl {
      url = "https://www.peppercarrot.com/0_sources/0ther/wallpapers/1920x1080/The-micro-dimension_peppercarrot-wallpaper_by-David-Revoy.jpg";
      sha256 = "1bmd9hlka2dnw8cdlayrs1grw11dviz0c0s5zlx7248qwia1j6rj";
    };
    dragon_e29 = fetchurl {
      url = "https://www.peppercarrot.com/0_sources/0ther/wallpapers/1920x1080/Episode-29-page-1_peppercarrot-wallpaper_by-David-Revoy.jpg";
      sha256 = "1fg694j79bmicw6zmppjm3wi7vhkh8dbka02bv5cpmrij7kafyv0";
    };
  };
  linuxPictures = {
    torSolarized = fetchurl {
      url = "https://raw.githubusercontent.com/LaniJW/linux-pictures/master/walls/solarized-dark/solarized-wallpaper-tor.png";
      sha256 = "0zk4d5qkflp23j8lqppafqnkkp8rj2ckxadj7p1qz7267x96jyx8";
    };
    kill9 = fetchurl {
      url = "https://linux.pictures/content/1-projects/201-welcome-to-linux-park/linux-park-demo.png";
      sha256 = "1v4sdfapm89v03h8vz0271rcz9f1ywpfpwhpg218078z0pmdfmmr";
    };
    hexcalendar = fetchurl {
      url = "https://linux.pictures/content/1-projects/226-one-root-to-rule-them-all-hex-calendar-2019/calendar-2019-root-demo.png";
      sha256 = "02jqrfjkvqp1gab29sq04wy3kvsxfi2jglfq1g0c8cscqpag4h4x";
    };
  };
  TLOS = rec {
    cynderWallpaper = (pkgs.fetchurl {
      url = "https://i.redd.it/uzfnpore9ie31.png";
      sha256 = "0c70j2l1ckx06cz8lr4kpq3rvrjds34yw15hq5bsa5wyr6pbrxzc";
    }).outPath;

    DOTDIs = (fetchgalleryDL {
      url = "https://shalonesk.deviantart.com/art/The-Legend-of-Spyro-DotD-is-151153490";
      sha256 = "1v0s780vn2m7d2hi95xh1p9vc2x0al056aag5is371q0lscym0dv";
    }).outPath;
  };
  konqiReignitedStyle = fetchurl {
    url = "https://d.facdn.net/art/draconis0868/1355172344/1355172344.draconis0868_konqi_01_stevemacintyre.png";
    sha256 = "0vm6yf6dp4vi97sr19qgmcczkmcm5p9klg81w2qx8y4drbs85ggr";
  };
  somatonic = {
    temple = fetchgalleryDL {
      url = "https://www.deviantart.com/somatonic/art/Temple-845445422";
      sha256 = "sha256-rza4G540BiKVQ6Q/tJIcIkzVdisAzGfkOtonjWzNHNQ=";
    };
  };
  mlp = rec {
    shysShores = (fetchgalleryDL {
      url = "https://www.deviantart.com/r1ftz/art/Shy-s-Shore-835359276";
      sha256 = "QT34SdZ4PMcsvr60Px/XeBtY/3/1V+j/33U7NCVcflU=";
    });
    teapartanimal = (fetchgalleryDL {
      url = "https://www.deviantart.com/drknz13/art/Tea-Party-Animals-701667784";
      sha256 = "P9Kt3LU6oLfcx0a5ZUXQp4MH9D2eiDKpDewGtrHU9PI=";
    });
    thankyou = (fetchgalleryDL {
      url = "https://www.deviantart.com/blazemizu/art/My-Little-Pony-Thank-you-816986332";
      sha256 = "sha256-8fF/TFW+hZME+UrEVeOA8MV6h84CtckRYGyGUQIavpk=";
    });
    hippogriffKingdom = (fetchgalleryDL {
      url = "https://www.deviantart.com/konsumo/art/The-Hippogriff-Kingdom-756255038";
      sha256 = "JS7Nk2cOxARiV1nmNpbpW+Zut23u1ae5h3izjP950Cs=";
    });

    sunnyalicorn = pkgs.stdenv.mkDerivation {
      name = "sunnyalicorn.png";
      
      src = (fetchgalleryDL {
        url = "https://www.spacecatsamba.com/misc/alt_sizes/sunny-alicorn-stream-wall-1920x1080.png";
        sha256 = "sha256-XR9my5wpKWaw/u9fawlBTK6Azk+irmB7LeZM3Ijn670=";
      });

      nativeBuildInputs = [ pkgs.imagemagick ];
      
      dontUnpack = true;

      installPhase = ''
        convert $src -fill "#ef9454" -draw "rectangle 0, 1026, 211, 1080" $out
      '';
    };

    izzy = fetchurl {
      url = "https://derpicdn.net/img/download/2022/5/7/2860796.jpg";
      sha256 = "sha256-1LjVaNRWf0NGTJtc8ieMSG+RG0m2kp8IbcMSGMXEaXU=";
    };

    #TODO: import from flake/channel
    loyaltyAnimated =
      let
        build_repo = pkgs.fetchFromGitHub {
          owner = "marius851000";
          repo = "MLP-loyalty-animated";
          rev = "a1f5d10f1d0ebbe9e7d1315b0a840dd2498c7fd7";
          sha256 = "sha256-I3CTxA/ts6neHxtg4exRc94CGSaO4OivbTzEA5h/2D8=";
        };
      in
      import build_repo { inherit pkgs; x = 1920; y = 1080; };
    red-forest-upscaled-webp = upscale_video {
      input = red-forest;
      name = "red-forest";
      additional_waifu_args = "-m noise-scale --noise-level 3";
    };

    # the plugin have an anoying wait when the animation end. Repeating the video a few time should make this appear rarely enought for it not to be a problem
    red-forest-upscaled-mp4 = let
      upscaled = upscale_video {
        input = red-forest;
        name = "red-forest";
        additional_waifu_args = "-m noise-scale --noise-level 3";
        extension = "mp4";
      }; #TODO: package the extension I'm using
    in
      pkgs.stdenv.mkDerivation {
        name = "red-forest-upscaled-mp4-scaled.mp4";
        dontUnpack = true;
        nativeBuildInputs = [ pkgs.ffmpeg-full ];
        buildPhase = "ffmpeg -stream_loop 30 -i ${upscaled} -c copy out.mp4";
        installPhase = "mv out.mp4 $out";
      };

    red-forest = pkgs.fetchurl {
      url = "https://derpicdn.net/img/download/2021/5/2/2606077.gif";
      sha256 = "sha256-yrZJKt4nyvPCY+dVSKR9TIUU/A8CUltwI751SVoR3J4=";
    };
  };
}
