{ pkgs, config, ... }:

let
  translations = {
    "virtualbox.desktop" = "Boite Virtuelle";
    "fish.desktop" = "poisson";
    "boinc.desktop" = "IOBCR";
    "insomnia.desktop" = "insomnie";
    "kdesystemsettings.desktop" = "Configuration de système de EBS";
    "keepass.desktop" = "gardien d’MDP";
    "kde_wacom_tabletfinder.desktop" = "Découvreur de tabletter de VER";
    "nixos-manual.desktop" = "Manuel de NeigeSE";
    "nvidia-settings.desktop" = "Configuration des graphhiques de Nenvie";
    "OpenRGB.desktop" = "RVB ouvert";
    "org.gnome.baobab.desktop" = "Baobab";
    "org.kde.dolphin.desktop" = "Dauphin";
    "org.kde.elisa.desktop" = "Élise";
    "steam.desktop" = "Vapeur";
    "osu.desktop" = "Yo!";
    "org.kde.ark.desktop" = "Arche";
    "org.kde.krita.desktop" = "Stylet";
    "org.strawberrymusicplayer.strawberry.desktop" = "Fraise";
    "dolphin-emu.desktop" = "L’autre dauphin";
  };

  pyscript = builtins.toFile "translate-desktop-files.py" ''
    import sys
    source_file = open(sys.argv[1])
    output_file = open(sys.argv[2], "w")
    new_name = sys.argv[3]
    current_section = None
    for line in source_file:
      reuse = True
      if line.startswith("["):
        current_section = line[1:].split("]")[0]
      elif line.startswith("#"):
        pass
      elif line.strip() == "":
        pass
      else:
        key = line.split("=")[0].strip().split("[")[0]
        value = line.split("=")[1].strip() # assume there is no equal in the value
        if key == "Name":
          reuse = False
          output_file.write(line.split("=")[0] + "=" + new_name + "\n")
          print(value)
          print(new_name)
      if reuse:
        output_file.write(line)
  '';
  translate_single_file = desktop_file: new_name: pkgs.runCommand
    "translated-desktop-file"
    {
      nativeBuildInputs = [ pkgs.python3 ];
    }
    ''
      python3 ${pyscript} ${desktop_file} $out ${pkgs.lib.escapeShellArg new_name}
    '';
  
  translate_package = package: 
    let
      paths = if (builtins.pathExists "${package}/share/applications") then
        let
          desktop_file_list = (builtins.attrNames /*(pkgs.lib.filterAttrs (k: v: (builtins.trace v v) == "regular")*/ (builtins.readDir "${package}/share/applications")/*)*/);
        in
          builtins.filter
            (x: x != null)
            (builtins.map (x:
              if (builtins.trace (translations ? "${x}") (translations ? "${x}")) then
                pkgs.runCommand
                  "translated-desktop-file-in-dir"
                  {}
                  ''
                    mkdir -p $out/share/applications
                    ln -s ${translate_single_file "${package}/share/applications/${x}" translations."${x}"} $out/share/applications/${x}
                  ''
              else
                null
            ) desktop_file_list)
      else [];
    in 
      if (builtins.length paths) != 0 then
        pkgs.buildEnv {
          name = "translated-desktops-${package.name}";
          inherit paths;
        }
      else
        pkgs.runCommand "empty" {} "mkdir $out";

  package_with_all_translation = let
    name = "translated-desktop-files";

    # filter out this package to avoid infinite recursion
    packages_to_translate = pkgs.lib.filter (x: x.name != name) config.environment.systemPackages;

    grouped = (pkgs.buildEnv {
      inherit name;

      paths = builtins.map (x: translate_package x) packages_to_translate;
    }) // {
      meta.priority = -10;
    };
  in grouped;
in {
  environment.systemPackages = [
#    (builtins.trace "${package_with_all_translation}" package_with_all_translation)
    package_with_all_translation
  ];
}