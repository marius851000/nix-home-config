{mlpicon}:

{ pkgs, ... }:

{
	environment.systemPackages = [
		(pkgs.callPackage "${mlpicon}/theme.nix" {})
	];
}
