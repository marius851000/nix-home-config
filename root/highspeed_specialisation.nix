{ config, pkgs, lib, ... }:
{
  specialisation.highspeed = {
    configuration = {
      nixpkgs.localSystem = {
        system = "x86_64-linux-linux";
        
        gcc.arch = "znver1";
        gcc.tune = "znver1";
      };

      /*services.xserver = {
        desktopManager.plasma5.enable = lib.mkForce false;
        desktopManager.xfce.enable = true;
      };*/
      #services.guix.enable = lib.mkForce false;
      services.tor.enable = lib.mkForce false;
      services.kubo.enable = lib.mkForce false;
      services.tor.client.enable = lib.mkForce false;
      virtualisation.lxd.enable = lib.mkForce false;
      virtualisation.docker.enable = lib.mkForce false;
      programs.adb.enable = lib.mkForce false;
      services.clamav = {
        updater.enable = lib.mkForce false;
        daemon.enable = lib.mkForce false;
      };
      services.mongodb.enable = lib.mkForce false;
    };
  };
}
