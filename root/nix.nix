{ config, pkgs, ...}:
{
  nix.package = pkgs.nixVersions.latest;/*.overrideAttrs (old: {
    src = pkgs.fetchFromGitHub {
      owner = "marius851000";
      repo = "nix";
      rev = "0c28bf4bb63dcade6c1768c2ee05ba645c57182a";
      sha256 = "sha256-ZjoJJZSZKRrDu+3fmzJQBNW8xp/QKIFcNUvdAM0Q6Oo=";
    };
  });*/

  #nix.package = pkgs.nixUnstable;

  nix.extraOptions = ''
    experimental-features = nix-command flakes ca-derivations
    keep-outputs = true
    keep-derivations = true
  '';

  /*nix.binaryCaches = [
    "https://cache.ngi0.nixos.org"
  ];

  nix.binaryCachePublicKeys = [
    "cache.ngi0.nixos.org-1:KqH5CBLNSyX184S9BKZJo1LxrxJ9ltnY2uAs5c/f1MA="
  ];
  nixpkgs.config.contentAddressedByDefault = true;*/
}
