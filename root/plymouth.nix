{ niximport_function }:

{ config, pkgs, ...}:

let
	niximport = niximport_function pkgs;
	cmc_spin_plymouth = pkgs.stdenv.mkDerivation {
		name = "cmc_spin_plymouth";
		src = ./data/cmc_spin.tar.bz; #from https://www.gnome-look.org/p/1000027/

		patchPhase = ''
		mv cmc.plymouth cmc_spin.plymouth
		substituteInPlace cmc_spin.plymouth \
			--replace /lib/ $out/share/
		'';

		installPhase = ''
			mkdir -p $out/share/plymouth/themes/cmc_spin
			cp -rf * $out/share/plymouth/themes/cmc_spin
		'';
	};

	vinyl_plymouth = pkgs.stdenv.mkDerivation {
		name = "vinyl_spin_plymouth";
		src = ./data/vinyl.tar.bz;

		patchPhase = ''
			substituteInPlace vinyl.plymouth \
				--replace /lib/ $out/share/
		'';

		installPhase = ''
			mkdir -p $out/share/plymouth/themes/vinyl
			cp -rf * $out/share/plymouth/themes/vinyl
		'';
	};

	static_image_plymouth = name: image: let
		installationFolder = "$out/share/plymouth/themes/${name}";
	in pkgs.stdenv.mkDerivation {
		name = "plymouth-image-${name}";
		src = ./data/static_image;

		nativeBuildInputs = [ pkgs.imagemagick ];

		patchPhase = ''
			mv static_image.plymouth ${name}.plymouth
			mv static_image.script ${name}.script
			substituteInPlace ${name}.plymouth \
				--subst-var-by Name ${name} \
				--subst-var-by ImageDir ${installationFolder} \
				--subst-var-by ScriptFile ${installationFolder}/${name}.script 
		'';

		installPhase = ''
			mkdir -p ${installationFolder}
			cp -rf * ${installationFolder}
			convert ${image} -resize ${config.boot.loader.grub.gfxmodeEfi}! ${installationFolder}/img.png
		'';
	};
in {
	boot.plymouth = {
		enable = true;
		themePackages = [
			vinyl_plymouth
			cmc_spin_plymouth
			(
				static_image_plymouth
				"grub_background"
				config.boot.loader.grub.splashImage
			)
			niximport.plymouth-nixos-pony.plymouth_theme
		];
		theme = "nixos-pony";
	};
}
