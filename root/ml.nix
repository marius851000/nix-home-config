{ config, pkgs, ... }:
#TODO: upstream changes.

let
	stdenv = pkgs.stdenv;

	gast_0_3_2 = pypack: pypack.gast.overridePythonAttrs (attrs: rec {
			version = "0.3.2";
			src = pypack.fetchPypi {
				inherit version;
				pname = attrs.pname;
				sha256 = "XHYX8fbIuLQmgZZCsWuQFnJ92uzRavmgd1PlN+uoo6U=";
			};
		});

	tensorflow_estimator_2_1_0 = pypack: pypack.tensorflow-estimator_2.overridePythonAttrs(attrs: rec {
			version = "2.1.0";
			src = pypack.fetchPypi {
				inherit version;
				pname = "tensorflow_estimator";
				format = "wheel";
				sha256 = "5cX2SKY28Y0b5M9+1GEysQii8PP9nxyFDrqSQmPcaXI=";
			};
		});

	tensorflow-estimator_1_14_0 = pypack: pypack.tensorflow-estimator_1.overridePythonAttrs(attrs: rec {
		version = "1.14.0";
		src = pypack.fetchPypi {
			inherit version;
			pname = "tensorflow_estimator";
			format = attrs.format;
			sha256 = "ygc/ZgY0B6CR1hDsGyLjnqMCSHEBmMxvE3aTIL2+OZI=";
		};
	});

	tensorflow-tensorboard_1_14_0 = pypack: pypack.tensorflow-tensorboard_1.overridePythonAttrs(attrs: rec {
		version = "1.14.0";
		src = pypack.fetchPypi {
			inherit version;
			pname = "tensorboard";
			format = "wheel";
			python = "py3";
			sha256 = "UOCxvc1Ijb45/ZQWl24Imy/03xjZ8fq0er9MSYIJw/w=";
		};

		pythonImportsCheck = ["tensorboard"];
	});

	scipy_1_4_1 = pypack: pypack.scipy.overridePythonAttrs(attrs: rec {
			version = "1.4.1";
			src = pypack.fetchPypi {
				inherit version;
				pname = attrs.pname;
				sha256 = "3uG786bI9ztrIYyyju2N0TNH6i+H1XLOGbKJ1v0/vFk=";
			};
			doCheck = false;
		});

	urlobject = pypack: pypack.buildPythonPackage rec {
		pname = "URLObject";
		version = "2.4.3";

		src = pypack.fetchPypi {
			inherit pname version;
			sha256 = "R7LiDmq5yDZrL0o1Zrb/QFMCXa0xHEu3Enm7z6JDDKo=";
		};
	};

	gdown = pypack: pypack.buildPythonPackage rec { #TODO: the version install in nixpkgs is the cmd one
		pname = "gdown";
		version = "3.12.2";

		src = pypack.fetchPypi {
			inherit pname version;
			sha256 = "4b3a1301e57bfd8dce939bf25ef8fbb4b23967fd0f878eede328bdcc41386bac";
		};

		doCheck = false;

		propagatedBuildInputs = with pypack; [ filelock requests tqdm setuptools ];
	};

	tensorboardx_latest = pypack: pypack.tensorboardx.overridePythonAttrs(attrs: rec {
		#version = "1.9";
		#src = pkgs.fetchFromGitHub {
  	#	owner = "lanpa";
  	#	repo = "tensorboardX";
	  #	rev = "v${version}";
  	#	sha256 = "MRr2kg2cgZxRloFEg4w4jHpqtuVKJE/JgCYu+2Z4GQU==";
  	#};
		doCheck = false; #TODO: fix
	});

	praatio = pypack: pypack.buildPythonPackage rec {
		pname = "praatio";
		version = "4.2.0";

		src = pypack.fetchPypi {
			inherit pname version;
			sha256 = "kVxVzDi1UWObxRhHyjIxZfZU3/37Fr0G+42t+uckS6A=";
		};
	};

	praat-parselmouth = pypack: pypack.buildPythonPackage rec {
		pname = "praat-parselmouth";
		version = "0.3.3";

		src = pkgs.fetchFromGitHub {
			owner = "YannickJadoul";
			repo = "Parselmouth";
			rev = "v${version}";
			sha256 = "kcg//dFK+g0X9Bs6TNrmnKA2UTOSSkqf0XgtK6pB60o=";
		};

		dontUseCmakeConfigure = true; # let python handle cmake

		nativeBuildInputs = [ pkgs.cmake ];
		buildInputs = [ pkgs.espeak pkgs.glpk pkgs.gsl pkgs.flac pkgs.portaudio ];
		propagatedBuildInputs = [ pypack.numpy ];
	};

	textgrid = pypack: pypack.buildPythonPackage rec {
		pname = "TextGrid";
		version = "1.5";

		src = pypack.fetchPypi {
			inherit pname version;
			sha256 = "eMRZ2pCi0rjFtmvPeBYLxVF1GN9gcHErjZIivyr4fJw=";
		};

		doCheck = false;

		pythonImportcheck = [ "textgrid" ];
	};

	alignment = pypack: pypack.buildPythonPackage rec {
		pname = "alignment";
		version = "1.0.10";

		src = pypack.fetchPypi {
			inherit pname version;
			sha256 = "IOxh4GPPGDkSO5bweV0Mfg+VNV//R9HpVeQILNBMJX4=";
		};

		propagatedBuildInputs = with pypack; [ six numpy ];
	};

	#TODO: override the one in nixpkgs
	openfst = stdenv.mkDerivation rec {
		pname = "openfst";
		version = "for_kaldi";

		src = pkgs.fetchFromGitHub {
			owner = "kkm000";
			repo = "openfst";
			rev = "0bca6e76d24647427356dc242b0adbf3b5f1a8d9"; # tag win/1.7.2.1
			sha256 = "PYdg7VNDG12ka2q+fN/abIpAlAsdqKJ4oH8ARULOAqA=";
		};

		enableParallelBuilding = true;
	};

	kaldi = stdenv.mkDerivation rec { #warning: this take an insane amount of disk space (try to find how to reduce this)
		pname = "kaldi";
		version = "latest";

		src = pkgs.fetchFromGitHub {
			owner = "kaldi-asr";
			repo = "kaldi";
			rev = "bcd163c5ae45a9dcc488c86e98281649b8156529";
			sha256 = "bsnz2wMQnIBNutYN5ioHj2f0enXPt4QhhS3n5awuv6Y=";
		};

		configurePhase = ''
			cd src
			substituteInPlace configure \
				--replace \$OPENFST_VER_NUM 10700 \
				--replace /bin/bash ${pkgs.bash}/bin/bash

			substituteInPlace base/get_version.sh \
				--replace "/usr/bin/env bash" ${pkgs.bash}/bin/bash

			#TODO: understand this
			#substituteInPlace matrix/Makefile \
			#	--replace matrix-lib-test ""

			./configure \
				--cub-root=${pkgs.cudatoolkit_11}/include \
				--mathlib=MKL \
				--mkl-root=${pkgs.mkl} \
				--fst-root=${openfst} \
				--cudatk-dir=${pkgs.cudatoolkit_11}
		'';

		installPhase = ''
			rm *bin/*.cc
			rm *bin/*.o
			rm *bin/Makefile
			rm *bin/CMakeLists.txt
			rm -rf onlinebin
			mkdir -p $out/bin
			cp *bin/* $out/bin
		'';

		makeFlags = [ "--no-silent" "LDFLAGS=-L${pkgs.gfortran8.cc.lib}/lib" ];

		nativeBuildInputs = [ pkgs.which pkgs.gcc8 pkgs.python pkgs.pkg-config ];

		buildInputs = [ openfst pkgs.cudatoolkit_11 pkgs.gfortran8 pkgs.mkl ];
		enableParallelBuilding = true;
	}; #TODO: use mkl instead
	#TODO: check if gcc is required at run-time
	#TODO: absolutly enable cuda

	mfa = pypack: let
		mfa_called_command = [ openfst kaldi ];

		executables = [ "align" "download" "g2p" "mfa" "train_and_align" "train_g2p" "validate" ]; #from aligner/command_line
		#executables = [ "align" "generate_dictionary" "train_and_align" "train_g2p" ];
	in pypack.buildPythonApplication rec {
		pname = "montreal_forced_aligner";
		version ="latest";
		#version = "1.0.1";

		src = pkgs.fetchFromGitHub {
			owner = "MontrealCorpusTools";
			repo = "Montreal-Forced-Aligner";
			rev = "4f8733409e79a50744616921a04fccf115e8af6f";
			sha256 = "xxC1nFD/TagkWKehkbe0RIX8sVrvH/tX3jfo+oqtpt0=";
		#	rev = "v${version}";
		#	sha256 = "Ci+yXjKcruYyCh4+kyjowS223p7voqyTns4bM+Af1j8=";
		};

		prePatch = ''
			substituteInPlace aligner/multiprocessing.py \
				--replace "'--dim=' + str(config.lda_dimension)," "'--dim=' + str(config.lda_dimension), '--allow-large-dim',"
		'';
		
		preInstall = ''
			mkdir -p $out/bin
			#cp aligner/command_line/* $out/bin
			${pkgs.lib.concatStringsSep "\n"
				(map (bin: let
						binpath = "$out/bin/mfa_${bin}";
					in ''cp aligner/command_line/${bin}.py ${binpath}
					chmod +x ${binpath}
					sed -i '1 s/^/#! \/bin\/env python\n/' ${binpath}
					wrapProgram ${binpath} --prefix PATH : ${pkgs.lib.makeBinPath mfa_called_command}
					'')
					executables
				)
			} #TODO: rewrite as a bash loop

			wrapPythonPrograms
		'';

		postFixup = pkgs.lib.concatStringsSep "\n"
			(map (bin: let
					binpath = "$out/bin/mfa_${bin}";
				in "wrapProgram ${binpath} --prefix PATH : ${pkgs.lib.makeBinPath mfa_called_command}")
				executables
			);

		nativeBuildInputs = [ pypack.wrapPython ];

		checkInputs = with pypack; [ pytest_4 ] ++ mfa_called_command; #TODO: wrap too
		doCheck = false; #TODO: enable those check
		propagatedBuildInputs = with pypack; [ pyyaml tqdm (textgrid pypack) (alignment pypack) requests ];
	};

	adabelief-pytorch = pypack : pypack.buildPythonPackage {
		pname = "adabelief-pytorch";
		version = "0.2.1";

		#TODO: better stuff, why not autogenerator ?
		src = pkgs.fetchurl {
			url = "https://files.pythonhosted.org/packages/cd/be/91bf777d5dc78b358d9ed3f0b6a5df2e28fa4a4be6e3f16f8e9ed66595b1/adabelief_pytorch-0.2.1.tar.gz";
			sha256 = "sha256-JcgDyg8UzPq+RwQAd7sOh+KEAezCbhSwa2YRZbA1NU8=";
		};

		propagatedBuildInputs = with pypack; [
			colorama
			pytorch-bin
			tabulate
		];

		doCheck = false;
	};

	kornia = pypack: pypack.buildPythonPackage {
		pname = "kornia";
		version = "0.6.5";
		
		src = pkgs.fetchFromGitHub {
			owner = "kornia";
			repo = "kornia";
			rev = "af025f508dfdfb2049a596f47e5ef20bc1924f7b";
			sha256 = "sha256-O4IJb5EGY1C7ZpILkdmv8aOrOgCC20WsA0Ufm6YeCJg=";
		};

		nativeBuildInputs = [
			pypack.pytest
			pypack.pytest-runner
		];

		propagatedBuildInputs = with pypack; [
			pytorch-bin
			retry
			packaging
		];
		
		doCheck = false;
	};

	ligthweight_gan = pypack: pypack.buildPythonApplication {
		pname = "lightweight-gan";
		version = "git";

		src = pkgs.fetchFromGitHub {
			owner = "lucidrains";
			repo = "lightweight-gan";
			rev = "2e60d413ece8baea70f9a0c6b2f4cfb18efd7aff";
			sha256 = "sha256-cJS4QEfT5CYqL0KPamNXnBlRm2ThvY2zAGieLGO12OE=";
		};

		patchPhase = ''
			substituteInPlace lightweight_gan/lightweight_gan.py \
				--replace "'jpg' if not self.transparent else 'png'" "'png'" \
				--replace "use_aim = True" "use_aim = False"
		'';

		propagatedBuildInputs = with pypack; [
			numpy
			pillow
			pytorch-bin
			(torchvision.override {
				pytorch = pytorch-bin;
			})
			tqdm
			fire
			einops
			(adabelief-pytorch pypack)
			(kornia pypack)
		];

		doCheck = false;
	};


	python_to_override = tensorflow_major: pypack: {
		#scipy = (scipy_1_4_1 pypack);
		#gast = (gast_0_3_2 pypack);
		#tensorflow-estimator_2 = (tensorflow_estimator_2_1_0 pypack);
		#tensorflow-estimator_1 = (tensorflow-estimator_1_14_0 pypack);
		#tensorflow-tensorboard_1 = (tensorflow-tensorboard_1_14_0 pypack);
		/*flake8 = pypack.flake8.overridePythonAttrs (attrs: {
			doCheck = false;
		});*/
		/*scikitlearn = pypack.scikitlearn.overridePythonAttrs (attrs: rec {
			doCheck = false;
		});*/ #TODO: fix this someway (maybe update the binary package)
		tensorflow = if tensorflow_major == 2 then
			(pypack.tensorflow-bin_2.override {
				cudaSupport = true;
				#cudatoolkit = pkgs.cudatoolkit_10_1;
			})
		else (
			pypack.tensorflow-bin_1.override {
				#cudaSupport = true;
				#cudatoolkit = pkgs.cudatoolkit_10_0;
				#cudnn = pkgs.cudnn_cudatoolkit_10_0;
			}
		);
		tensorflow-tensorboard = if tensorflow_major == 2 then pypack.tensorflow-tensorboard_2 else pypack.tensorflow-tensorboard;
		pytorch = pypack.pytorch-bin;
		tensorboardx = (tensorboardx_latest pypack);
	};

	override_python = tensorflow_major: pypack: pypack.override ({ #TODO: use cleaner overriding (read the doc)
			overrides = self: super: python_to_override tensorflow_major super;
		});

	python_package_for_ml = tensorflow_major: pypack: with (override_python tensorflow_major pypack); [
		#gdown
		ipykernel
		ipywidgets
		widgetsnbextension
		tensorflow
		imageio

		Keras
		#pandas
		/*(pypack.tensorflow-bin_2.override {
			cudaSupport = true;
			cudatoolkit = pkgs.cudatoolkit_10_1;
		})*/
		#tensorboardx
	 	pytorch
		#librosa
		matplotlib
		pip
		#numpy
		#scipy
		tqdm
		torchvision
		#seaborn
		#flask
		#numba
		#llvmlite
		#inflect
		#unidecode
		#setuptools
		#pillow

		#h5py

		#for mlp syntesis collab script
		#(urlobject pypack)
		#(gdown pypack)
		#pycrypto
		#(praatio pypack)
		#(praat-parselmouth pypack)
	];

	python_for_ml = tensorflow_major: pkgs.python39.withPackages (python_package_for_ml tensorflow_major);

	kernels = pkgs.lib.mapAttrs (n: v: {
			displayName = "python 3 for ML";
			argv = [
				"${(python_for_ml v).interpreter}"
				"-m"
				"ipykernel_launcher"
				"-f"
				"{connection_file}"
			];
			language = "python";
		}) {
			#python3ForMLWithTensorflow1 = 1;
			python3ForMLWithTensorflow2 = 2;
		};
in {
	services.jupyter = {
		enable = false;
		user = "marius";
		#lets hope it stay behind a firewall, and your computer isn't multi user
		password = "espace";
		kernels = kernels;
	};

	environment.systemPackages = with pkgs.python39Packages; [
		jupyter_core notebook #(mfa (override_python 2 pkgs.python37Packages))
	] ++ [ pkgs.megatools pkgs.sox pkgs.python39Packages.pip (ligthweight_gan pkgs.python39Packages) ];
}
