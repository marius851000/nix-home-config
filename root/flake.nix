{
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable-small";
  #inputs.nixpkgs.url = "nixpkgs";
  #inputs.nixpkgs.url = "github:NixOs/nixpkgs/tabby";
  #inputs.nixpkgs.url = "nixpkgs/68a8cdf";
  inputs.nixos-hardware.url = "nixos-hardware";
  inputs.ricenur.url = "github:marius851000/ricenur";
  #inputs.ricenur.url = "/home/marius/ricenur";
  inputs.ricenur_non_flake = {
    url = "github:marius851000/ricenur";
    flake = false;
  };
  #TODO: convert all of ricenur to flake?
  inputs.mlpicon = {
    url = "github:marius851000/mlpicon";
    flake = false;
  };
  inputs.guix = {
    url = "github:Emiller88/guix";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  inputs.envfs = {
    url = "github:Mic92/envfs";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  inputs.home-manager = {
    url = "github:nix-community/home-manager";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  inputs.mlpnur = {
    url = "github:marius851000/mlpnur";
    flake = false;
  };
  inputs.mariusnur = {
    url = "git+https://framagit.org/marius851000/mariusnur.git";
    flake = false;
  };
  inputs.funkinnur.url = "github:marius851000/funkinnur";
  inputs.plymouth-nixos-pony-src = {
    url = "github:marius851000/nixos-pony";
    flake = false;
  };
  inputs.nix-snapshotter = {
    url = "github:pdtpartners/nix-snapshotter";
    inputs.nixpkgs.follows = "nixpkgs";
  };
  /*inputs.nixcitizen = {
    url = "git+https://gitea.xndr.de/philipp/nixcitizen.git?ref=main";
    flake = false;
  };*/

  #TODO: niximport as overlay;
  outputs =
    { self
    , nixpkgs
    , nixos-hardware
    , ricenur
    , mlpicon
    , guix
    , envfs
    , home-manager
    , mlpnur
    , mariusnur
    , ricenur_non_flake
    , funkinnur
    , plymouth-nixos-pony-src
    , nix-snapshotter
      #, nixcitizen
    }:
    let
      base_import = niximport_function: [
        (import ./configuration.nix { })
        (import ./plymouth.nix { inherit niximport_function; })
        ./binfmt.nix
        ./nix.nix
        (import ./desktop/mlpicon.nix { inherit mlpicon; })
        ricenur.nixosModules.plasma_advanced_radio
        ricenur.nixosModules.animated_wallpaper
        ricenur.nixosModules.panon
        # ./neigese-traduction-abusive.nix
        # home-manager
        home-manager.nixosModules.home-manager
        (
          { config, pkgs, ... }:
          {
            programs.steam.enable = true;
            #temporary test
            services.yggdrasil = {
              enable = true;
              settings = {
                Peers = [
                  "tls://mariusdavid.fr:6509"
                ];
                MulticastInterfaces = [
                  {
                    Port = 34023;
                  }
                ];
              };
              persistentKeys = true;
              openMulticastPort = true;
            };
            networking.firewall.allowedTCPPorts = [ 34023 ];

            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            services.flatpak.enable = true;
            home-manager.extraSpecialArgs = {
              niximport = niximport_function pkgs;
            };
            nixpkgs.config.permittedInsecurePackages = [
              "libav-11.12"
              "ffmpeg-2.8.17"
              "ffmpeg-3.4.8"
              "erlang-22.3.4.24"
            ];
          }
        )
      ];

      niximport = profile: pkgs: import ../niximport.nix {
        inherit pkgs profile;
        mariusnur_src = mariusnur;
        mlpnur_src = mlpnur;
        ricenur_src = ricenur_non_flake;
        funkinnur_flake = funkinnur;
        plymouth-nixos-pony-src = plymouth-nixos-pony-src;
      };
    in
    {
      nixosConfigurations.marius-tuf = let
        profile = import ../profiles/home.nix;
        niximport_function = niximport profile;
      in nixpkgs.lib.nixosSystem rec {
        system = "x86_64-linux";
        modules = [
          profile.nixos
          /*./kubernetestest/kubernetes.nix
          ({ pkgs, ... }: {
            # (1) Import nixos module.
            imports = [ nix-snapshotter.nixosModules.default ];

            # (2) Add overlay.
            nixpkgs.overlays = [ nix-snapshotter.overlays.default ];

            # (3) Enable service.
            services.nix-snapshotter = {
              enable = true;
              setContainerdSnapshotter = true;
            };

            # (4) Add a containerd CLI like nerdctl.
            environment.systemPackages = [ pkgs.nerdctl pkgs.kubectl ];
          })*/
          #./ml.nix
          ./hardware/tuf.nix
          ./marius-hydra.nix
          (import ./grubtheme.nix { inherit niximport_function; })
          ./rss-bridge.nix
          #./rplace.nix
          nixos-hardware.nixosModules.common-pc-laptop-ssd
          nixos-hardware.nixosModules.common-pc-laptop
          nixos-hardware.nixosModules.common-cpu-amd
          nixos-hardware.nixosModules.common-gpu-nvidia
          ({ config, pkgs, ... }:
            {
              home-manager.users.marius = ../home.nix;
            }
          )
          (
            { config, pkgs, ... }:
            {
              services.clamav = {
                updater.enable = false;
                daemon.enable = true;
              };
            }
          )
          ./lemp.nix
        ] ++ (base_import niximport_function);
      };
    };
}
