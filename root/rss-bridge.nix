{ pkgs, ... }:

{
  services.rss-bridge =
    {
      enable = true;
      virtualHost = "rssbridge";
      whitelist = [
        "derpibooru"
      ];
    };

  services.nginx.virtualHosts.rssbridge = {
    listen = [
      {
        addr = "localhost";
        port = 85;
        ssl = false;
      }
    ];
  };

  #TODO: upstream this
  services.phpfpm.pools.rss-bridge.phpPackage = pkgs.php.withExtensions (
    { enabled, all }: with all; [
      openssl
      mbstring
      simplexml
      curl
      #json
      filter
    ]
  );
}
