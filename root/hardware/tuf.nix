{ config, lib, pkgs, modulesPath, ... }:

let
  kernel = config.boot.kernelPackages.kernel;

  faustus = with pkgs; stdenv.mkDerivation rec {
    name = "faustus-${kernel.version}-git";
    
    src = fetchFromGitHub {
      owner = "hackbnw";
      repo = "faustus";
      rev = "ab4c215564cacc8e3b84cec09b5df07147803179";
      sha256 = "sha256-rDssErlqPCZhWkfiItCKlGPfCPOoG+7EfooeZsXhKOI=";
    };

    preConfigure = ''
      export out_back=$out
    '';

    makeFlags = [
      "KERNELDIR=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build"
    ];

    installPhase = ''
      echo $out_back
      install -D /build/source/src/faustus.ko $out_back/lib/modules/${kernel.modDirVersion}/misc/faustus.ko
    '';
  };

    # TODO: do not commit, put in private...
  run-scylla = pkgs.writeScriptBin "run-scylla" ''
      docker run \
      -p 8100:8100 \
      -p 9042:9042 \
      --name scylla \
      --hostname test-scylla \
      -d scylladb/scylla \
      --smp 10 \
      -m8G
      --developer-mode 1 \
      --alternator-port 8100 \
      --alternator-address 0.0.0.0 \
      --alternator-write-isolation always
    '';
in {
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  nixpkgs.config.permittedInsecurePackages = [
    "python3.10-tensorflow-2.11.1"
    "tensorflow-2.11.1"
    "tensorflow-2.11.1-deps.tar.gz"
  ];
  
  environment.systemPackages = with pkgs; [
    openrgb
    pkgs.libnotify
    #tts
    #TODO: do not commit, put in private
    run-scylla
  ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "rtsx_pci_sdmmc" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ /*"8821ce"*/ ]; # actually, my connection issue was a fixed hardware issue (just unplug and replug the wireless network card)
  boot.kernelModules = [ "kvm-amd" ];
  boot.blacklistedKernelModules = [ "asus_nb_wmi" "asus_wmi" ];
  boot.extraModulePackages = with config.boot.kernelPackages; [
    v4l2loopback
    #rtl8821ce
    /*faustus*/
  ];

  #nix.settings.extra-sandbox-paths = [ "/tmp/cache/ccache" "/tmp/hxcpp-cache" ];

  services.boinc = {
    enable = true;
    extraEnvPackages = [ pkgs.virtualbox pkgs.ocl-icd pkgs.linuxPackages.nvidia_x11 ];
  };

  services.gnome.gnome-keyring.enable = true;
  services.replay-sorcery = {
    enable = true;
    settings = {
      recordSeconds = 60;
      videoInput = "x11";
      videoQuality = 17;
      audioInput = "pulse";
      audioDevice = "alsa_output.pci-0000_05_00.6.analog-stereo.monitor";
    };
    enableSysAdminCapability = true;
  };
  
  programs.nix-ld.enable = true;

  services.joycond.enable = false;
  
  /*boot.kernelPatches = [ {
    name = "wiiu-patch-mac-stuff";
    patch = ../wiiulinpatch-5-13-rc2.diff;
    extraConfig = "";
  } ];*/

  #networking.wireless.enable = lib.mkForce false;
  #networking.networkmanager.enable = lib.mkForce false;
  networking.networkmanager.enableStrongSwan = true;

  /*services.tts.servers = {
    english = {
      port = 5300;
      model = "tts_models/en/ljspeech/tacotron2-DDC";
      #useCuda = true;
    };
  };*/

  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/ed121f66-21c3-4e1c-8aba-74a1b2a743a2";
      fsType = "btrfs";
      #options = [ "compress-force" ];
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/60AB-B557";
      fsType = "vfat";
    };

  fileSystems."/hdd" = {
    device = "/dev/disk/by-uuid/1b748969-9e6f-4eea-b632-2bf976eb48f7";
    fsType = "btrfs";
    options = [ "nofail" ];
  };

  /*services.mysql = {
    enable = true;
    package = pkgs.mariadb;
    settings.mysqld.innodb_use_native_aio = "OFF";
  };*/

  sound.enable = lib.mkForce false;
  hardware.pulseaudio.enable = lib.mkForce false;

  security.rtkit.enable = true;
  services.pipewire = {
    wireplumber.enable = true;
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    #TODO: port that to the /etc/pipewire/pipewire.conf.d/
    /*config.pipewire = {
    "context.properties" = {
      "link.max-buffers" = 16;
      "default.clock.rate" = 48000;
      #"default.clock.rate" = 192000; #yep, I'm crazy
      "default.clock.quantum" = 48*8;
      "default.clock.min-quantum" = 48*8;
      "default.clock.max-quantum" = 48*8;
      #"default.clock.min-quantum" = 192*4;
      #"default.clock.max-quantum" = 192*4;
    };
    "context.modules" = [
      {
        name = "libpipewire-module-rtkit";
        args = {
          "nice.level" = -15;
          "rt.prio" = 88;
          "rt.time.soft" = 200000;
          "rt.time.hard" = 200000;
        };
        flags = [ "ifexists" "nofail" ];
      }
      { name = "libpipewire-module-protocol-native"; }
      { name = "libpipewire-module-profiler"; }
      { name = "libpipewire-module-metadata"; }
      { name = "libpipewire-module-spa-device-factory"; }
      { name = "libpipewire-module-spa-node-factory"; }
      { name = "libpipewire-module-client-node"; }
      { name = "libpipewire-module-client-device"; }
      {
        name = "libpipewire-module-portal";
        flags = [ "ifexists" "nofail" ];
      }
      {
        name = "libpipewire-module-access";
        args = {};
      }
      { name = "libpipewire-module-adapter"; }
      { name = "libpipewire-module-link-factory"; }
      { name = "libpipewire-module-session-manager"; }
    ];
  };*/
    jack.enable = true;
  };

  /*environment.etc."pipewire/pipewire.conf.d/surround.conf".text = ''
    # Convolver sink
#
# Copy this file into a conf.d/ directory such as
# ~/.config/pipewire/filter-chain.conf.d/
#
context.modules = [
    { name = libpipewire-module-filter-chain
        args = {
            node.description = "Virtual Surround Sink"
            media.name       = "Virtual Surround Sink"
            filter.graph = {
                nodes = [
                    # duplicate inputs
                    { type = builtin label = copy name = copyFL  }
                    { type = builtin label = copy name = copyFR  }
                    { type = builtin label = copy name = copyFC  }
                    { type = builtin label = copy name = copyRL  }
                    { type = builtin label = copy name = copyRR  }
                    { type = builtin label = copy name = copySL  }
                    { type = builtin label = copy name = copySR  }
                    { type = builtin label = copy name = copyLFE }

                    # apply hrir - HeSuVi 14-channel WAV (not the *-.wav variants) (note: *44* in HeSuVi are the same, but resampled to 44100)
                    { type = builtin label = convolver name = convFL_L config = { filename = "hrir_hesuvi/hrir.wav" channel =  0 } }
                    { type = builtin label = convolver name = convFL_R config = { filename = "hrir_hesuvi/hrir.wav" channel =  1 } }
                    { type = builtin label = convolver name = convSL_L config = { filename = "hrir_hesuvi/hrir.wav" channel =  2 } }
                    { type = builtin label = convolver name = convSL_R config = { filename = "hrir_hesuvi/hrir.wav" channel =  3 } }
                    { type = builtin label = convolver name = convRL_L config = { filename = "hrir_hesuvi/hrir.wav" channel =  4 } }
                    { type = builtin label = convolver name = convRL_R config = { filename = "hrir_hesuvi/hrir.wav" channel =  5 } }
                    { type = builtin label = convolver name = convFC_L config = { filename = "hrir_hesuvi/hrir.wav" channel =  6 } }
                    { type = builtin label = convolver name = convFR_R config = { filename = "hrir_hesuvi/hrir.wav" channel =  7 } }
                    { type = builtin label = convolver name = convFR_L config = { filename = "hrir_hesuvi/hrir.wav" channel =  8 } }
                    { type = builtin label = convolver name = convSR_R config = { filename = "hrir_hesuvi/hrir.wav" channel =  9 } }
                    { type = builtin label = convolver name = convSR_L config = { filename = "hrir_hesuvi/hrir.wav" channel = 10 } }
                    { type = builtin label = convolver name = convRR_R config = { filename = "hrir_hesuvi/hrir.wav" channel = 11 } }
                    { type = builtin label = convolver name = convRR_L config = { filename = "hrir_hesuvi/hrir.wav" channel = 12 } }
                    { type = builtin label = convolver name = convFC_R config = { filename = "hrir_hesuvi/hrir.wav" channel = 13 } }

                    # treat LFE as FC
                    { type = builtin label = convolver name = convLFE_L config = { filename = "hrir_hesuvi/hrir.wav" channel =  6 } }
                    { type = builtin label = convolver name = convLFE_R config = { filename = "hrir_hesuvi/hrir.wav" channel = 13 } }

                    # stereo output
                    { type = builtin label = mixer name = mixL }
                    { type = builtin label = mixer name = mixR }
                ]
                links = [
                    # input
                    { output = "copyFL:Out"  input="convFL_L:In"  }
                    { output = "copyFL:Out"  input="convFL_R:In"  }
                    { output = "copySL:Out"  input="convSL_L:In"  }
                    { output = "copySL:Out"  input="convSL_R:In"  }
                    { output = "copyRL:Out"  input="convRL_L:In"  }
                    { output = "copyRL:Out"  input="convRL_R:In"  }
                    { output = "copyFC:Out"  input="convFC_L:In"  }
                    { output = "copyFR:Out"  input="convFR_R:In"  }
                    { output = "copyFR:Out"  input="convFR_L:In"  }
                    { output = "copySR:Out"  input="convSR_R:In"  }
                    { output = "copySR:Out"  input="convSR_L:In"  }
                    { output = "copyRR:Out"  input="convRR_R:In"  }
                    { output = "copyRR:Out"  input="convRR_L:In"  }
                    { output = "copyFC:Out"  input="convFC_R:In"  }
                    { output = "copyLFE:Out" input="convLFE_L:In" }
                    { output = "copyLFE:Out" input="convLFE_R:In" }

                    # output
                    { output = "convFL_L:Out"  input="mixL:In 1" }
                    { output = "convFL_R:Out"  input="mixR:In 1" }
                    { output = "convSL_L:Out"  input="mixL:In 2" }
                    { output = "convSL_R:Out"  input="mixR:In 2" }
                    { output = "convRL_L:Out"  input="mixL:In 3" }
                    { output = "convRL_R:Out"  input="mixR:In 3" }
                    { output = "convFC_L:Out"  input="mixL:In 4" }
                    { output = "convFC_R:Out"  input="mixR:In 4" }
                    { output = "convFR_R:Out"  input="mixR:In 5" }
                    { output = "convFR_L:Out"  input="mixL:In 5" }
                    { output = "convSR_R:Out"  input="mixR:In 6" }
                    { output = "convSR_L:Out"  input="mixL:In 6" }
                    { output = "convRR_R:Out"  input="mixR:In 7" }
                    { output = "convRR_L:Out"  input="mixL:In 7" }
                    { output = "convLFE_R:Out" input="mixR:In 8" }
                    { output = "convLFE_L:Out" input="mixL:In 8" }
                ]
                inputs  = [ "copyFL:In" "copyFR:In" "copyFC:In" "copyLFE:In" "copyRL:In" "copyRR:In", "copySL:In", "copySR:In" ]
                outputs = [ "mixL:Out" "mixR:Out" ]
            }
            capture.props = {
                node.name      = "effect_input.virtual-surround-7.1-hesuvi"
                media.class    = Audio/Sink
                audio.channels = 8
                audio.position = [ FL FR FC LFE RL RR SL SR ]
            }
            playback.props = {
                node.name      = "effect_output.virtual-surround-7.1-hesuvi"
                node.passive   = true
                audio.channels = 2
                audio.position = [ FL FR ]
            }
        }
    }
]
'';*/

  hardware.openrazer = {
    enable = true;
    users = [ "marius" ];
    syncEffectsEnabled = false;
    keyStatistics = true;
  };

  #virtualisation.waydroid.enable = true;

  #services.gnome.at-spi2-core.enable = true;

  swapDevices =
    [
      { device = "/dev/disk/by-uuid/f8f32e52-610c-489e-afe7-987035bb26ed"; }
    ];

  services.beesd.filesystems.root = {
    spec = "/";
    hashTableSizeMB = 2048;
  };

  /*services.beesd.filesystems.hdd = {
    spec = "/hdd";
    hashTableSizeMB = 2048;
  };*/

  boot.loader.grub.gfxmodeEfi = "1920x1080";
  boot.loader.grub.device = "/dev/nvme0n1";

  hardware.nvidia.prime = {
    sync.enable = false;
    offload.enable = true;
    amdgpuBusId = "PCI:5:0:0";
    nvidiaBusId = "PCI:1:0:0";
  };
  
  # For Yuzu
  boot.kernel.sysctl = {
    "vm.max_map_count" = 65530*8;
  };

  boot.kernelPackages = pkgs.linuxPackages_latest;
  hardware.opengl.enable = true;
  #hardware.opengl.extraPackages = [ pkgs.rocm-opencl-icd pkgs.rocminfo pkgs.rocm-smi ];

  services.fwupd.enable = true;


  boot.extraModprobeConfig = ''
    options snd-hda-intel model=alc233-eapd
  '';

  services.input-remapper.enable = true;
  #services.input-remapper.enableUdevRules = true;

  services.kubo = {
    enable = true;
    dataDir = "/hdd/ipfsdata/";
    autoMount = true;
    settings.Addresses.Gateway = "/ip4/127.0.0.1/tcp/8085";
    startWhenNeeded = true;
    autoMigrate = true;
    settings.Addresses.API = [ "/ip4/127.0.0.1/tcp/5001" ];
  };
  #systemd.services.ipfs.serviceConfig.ReadWritePaths = lib.mkForce [ ]; # https://github.com/NixOS/nixpkgs/pull/166340

  services.sysprof.enable = true;
  
  systemd.services.ipfs.after = [ "hdd.mount" ];

  systemd.services.NetworkManager-wait-online.wantedBy = lib.mkForce [];

  services.flatpak.enable = true;
  #xdg.portal.enable = true;
  #xdg.portal.gtkUsePortal = true;

  services.couchdb = {
    enable = true;
    adminPass = "dontneedapasswordforlocalsystem";
    extraConfig = ''
      [couchdb]
      file_compression = snappy
      [database_compaction]
      doc_buffer_size = 10000000
      checkpoint_after = 20000000
    '';
  };

  hardware.sane = {
    enable = true;
    openFirewall = true;
  };

  /*services.gnome.at-spi2-core.enable = true;*/

  boot.loader.grub.extraEntries = ''
    menuentry "GParted live" {
      search --set=root --fs-uuid 42B0-4F0F
      linux /live-hd/vmlinuz boot=live config union=overlay username=user components noswap noeject vga=788 ip= net.ifnames=0 live-media-path=/live-hd bootfrom=/dev/disk/by-uuid/42B0-4F0F toram=filesystem.squashfs
      initrd /live-hd/initrd.img
    }
  '';
}
