{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "rtsx_pci_sdmmc" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = with config.boot.kernelPackages; [
    v4l2loopback
  ];

  services.gnome.at-spi2-core.enable = true;
  
  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/507fdbf7-5fcc-48f3-a3e8-0145d403dc1f";
      fsType = "ext4";
#      options = [ "compress-force" ];
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/CC9A-5A4E";
      fsType = "vfat";
    };

  boot.loader.grub.gfxmodeEfi = "1366x720";
  boot.loader.grub.device = "/dev/disk/by-id/usb-WD_Elements_2620_575854324537304538443753-0:0";
  boot.loader.grub.useOSProber = lib.mkForce false;
}
