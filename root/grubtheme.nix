{ niximport_function }:

{ config, pkgs, lib, ... }:

let
	niximport = niximport_function pkgs;
	
	spash = pkgs.stdenv.mkDerivation rec {
		name = "grub_marius_wallpaper.png";

		src = (pkgs.fetchurl {
			url = "https://www.mylittlewallpaper.com/images/o_52f528722734d4.60978064.png?download=1";
			sha256 = "VRshr59gIXFPSi51eqcWkTAwLYpcYrKFyxJ6D/bAM4Y=";
			name = "splash.png";
		});

		phases = [ "buildPhase" "installPhase" ];

		buildInputs = [ pkgs.imagemagick ];

		# remove the author mark and resize (spaceatsamba.com)
		# mirror here: https://www.deviantart.com/sambaneko/art/Applejack-Silhouette-Wall-372255076
		buildPhase = ''
			convert ${src} -fill "#fec46e" -draw "rectangle 0,1718 400,1800" -resize ${config.boot.loader.grub.gfxmodeEfi}! splash.png
		'';

		installPhase = "mv splash.png $out";
	};

	splash_second = pkgs.stdenv.mkDerivation rec {
		name = "grub_marius_wallpaper_nixos.png";
		
		src = pkgs.fetchurl {
			url = "https://derpicdn.net/img/download/2021/3/6/2565596.png";
			sha256 = "sha256-3IXS37aVjs3r3cyxDv/LZWbPyirD43rk/rZbYpGJlTM=";
		};

		phases = [ "buildPhase" "installPhase" ];

		buildInputs = [ pkgs.imagemagick ];

		buildPhase = ''
			convert ${src} -resize ${config.boot.loader.grub.gfxmodeEfi}! splash.png
		'';

		installPhase = ''
			cp splash.png $out
		'';
	};

	splash_zipp = pkgs.stdenv.mkDerivation rec {
		name = "grub_wallpaper_again.png";

		src = pkgs.fetchurl {
			url = "https://derpicdn.net/img/download/2021/10/1/2713397.jpg";
			sha256 = "0l85f3vqik2vd1rp4356y243rp8q96vf3g202bz6bdxbkn7d2ryf";
		};

		dontUnpack = true;

		buildInputs = [ pkgs.imagemagick pkgs.waifu2x-converter-cpp ];

		buildPhase = ''
			convert ${src} -fill "#f1edea" -draw "rectangle 0,680 100,720" uncredited.png
			waifu2x-converter-cpp -i uncredited.png -m noise-scale --noise-level 2 -o upscaled.png
			convert upscaled.png -resize ${config.boot.loader.grub.gfxmodeEfi}! splash.png
		'';

		installPhase = "mv splash.png $out";
	};

	sunny_belle = pkgs.stdenv.mkDerivation rec {
		name = "sunny_belle.png";

		src = ./sunnybelle.jpg;

		dontUnpack = true;

		buildInputs = [ pkgs.imagemagick ];

		buildPhase = ''
			convert ${src} -resize ${config.boot.loader.grub.gfxmodeEfi}! splash.png
		'';

		installPhase = "mv splash.png $out";

		extraConfig = ''
			set menu_color_highlight=black/white
			set menu_color_normal=green/black
			set color_normal=green/black
		'';
	};

	resize_for_grub = image: pkgs.stdenv.mkDerivation rec {
		name = "${image.name}-scaled-for-grub";

		src = image;

		dontUnpack = true;

		buildInputs = [ pkgs.imagemagick ];

		buildPhase = ''
			convert ${src} -resize ${config.boot.grub.gfxmodeEfi}! splash.png
		'';

		installPhase = "mv splash.png $out";
	};

	#instruction from the arch wiki
	patch_theme = theme: image: pkgs.stdenv.mkDerivation {
		name = "patch_sddm_theme_for_image";

		dontUnpack = true;
		#TODO: maybe convert to png ? For grub too.

		installPhase = ''
			mkdir -p $out/share/sddm/themes/${theme}/
			cd $out/share/sddm/themes/${theme}
			echo "[General]
			background=${image}" > theme.conf.user
		'';
	};

	#cool_image_of_the_month = niximport.images.mlp.izzy;
	cool_image_of_the_month = niximport.plymouth-nixos-pony.static_image_gray;

in {
	boot.loader.grub.splashImage = cool_image_of_the_month;
	environment.systemPackages = [ (patch_theme "breeze" niximport.plymouth-nixos-pony.static_image_color) ];


	boot.kernelParams = [ "quiet" "systemd.show_status=auto" "udev.log_level=3" "splash" "video=efifb:nobgrt" /*"plymouth:debug"*/];
	boot.consoleLogLevel = 0;
	boot.initrd.verbose = false;

	systemd.services.setloglevel = {
		serviceConfig = {
			ExecStart = "${pkgs.coreutils}/bin/echo \"6\" > /proc/sys/kernel/printk";
			Type = "oneshot";
		};
		wants = [ "multi-user.target" ];
		wantedBy = [ "graphical.target" ];
	};
}
