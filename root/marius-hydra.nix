{ pkgs, config, ... }:


let
  git-with-lfs = pkgs.stdenv.mkDerivation {
    pname = "git-with-lfs";
    version = "git-${pkgs.git.version or "unspecified"}-git-lfs-${pkgs.git-lfs.version or "unspecified"}";

    phases = [ "installPhase" ];

    nativeBuildInputs = [ pkgs.makeWrapper ];
    buildInputs = [ pkgs.git pkgs.git-lfs ];

    installPhase = ''
      mkdir -p $out/bin
      makeWrapper ${pkgs.git}/bin/git $out/bin/git \
        --add-flags "-c" --add-flags "filter.lfs.clean=\"git-lfs clean -- %f\"" \
        --add-flags "-c" --add-flags "filter.lfs.process=\"git-lfs filter-process\"" \
        --add-flags "-c" --add-flags "filter.lfs.required=true" \
        --add-flags "-c" --add-flags "filter.lfs.smudge=\"git-lfs smudge -- %f\"" \
        --prefix PATH : "${pkgs.lib.makeBinPath [ pkgs.git-lfs ]}"
    '';
  };

  git-with-lfs-for-hydra = let
    wrapper = pkgs.writeScript "git-lfs-wrapper.sh" ''
      #!${pkgs.bash}/bin/bash
      >&2 echo using git
      >&2 echo args : $@
    '';

  in pkgs.stdenv.mkDerivation {
    pname = "git-with-lfs-for-hydra";
    version = git-with-lfs.version;

    phases = [ "installPhase" ];

    nativeBuildInputs = [ pkgs.makeWrapper ];
    
    installPhase = ''
      mkdir -p $out/bin
      makeWrapper ${wrapper} $out/bin/git \
        --prefix PATH : ${pkgs.lib.makeBinPath [ git-with-lfs ]}
    '';
  };

  
in {
  services.hydra = {
    enable = false;
    hydraURL = "http://localhost:3000";
    notificationSender = "hydra@localhost";
    buildMachinesFiles = [ ];
    useSubstitutes = true;
    package = pkgs.hydra-unstable.overrideAttrs (old: {
      patchPhase = (old.patchPhase or "") + ''
        substituteInPlace src/hydra-eval-jobs/hydra-eval-jobs.cc \
          --replace "restrictEval = true;" "restrictEval = false;" \
          --replace "pureEval = myArgs.flake;" "pureEval = false;"
      '';
      hydraPath = pkgs.lib.makeBinPath ([ old.hydraPath pkgs.git-lfs ]); # twice to ensure this replace the already present git
    });
  };

/*    nix.buildMachines = [
    {
    hostName = "ssh://localhost";
    systems = [ "armv7l-linux" "x86_64-linux" "armv6l-linux" "aarch64-linux" "i686-linux" ];
    maxJobs = 8;
    supportedFeatures = [ "nixos-test" "kvm" "nixos-test" "big-parallel" ];
    }
    ];*/

  nix.distributedBuilds = true;

  services.openssh = {
    enable = true;
    permitRootLogin = "yes";
  };

  environment.systemPackages = [
    pkgs.bash
  ];
}
