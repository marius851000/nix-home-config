{ ricenur }:

{ config, pkgs, ... }:
# keep the minimal to install nixos with flake/experimental here. Allow to
# install without flake support on the installer.

with pkgs;
let
  my-python-package = python-packages: with python-packages; [
    wget
    requests
    lxml
    sqlalchemy
    pyqt5
    redis
    docopt
    numpy
    websockets
    pyaudio
    pillow
    cffi
  ];
  python-with-my-package = python3.withPackages my-python-package;

  ricePkgs = (import ricenur { inherit pkgs; }).ricePkgs;
in
{
  security.pki.certificateFiles = [
    "${pkgs.fetchurl {
      url = "https://www.thawte.com/roots/thawte_Primary_Root_CA.pem";
      sha256 = "1rasagyq14a43dgrb833va6y1ksxvjiypf8srw2sn9qibv6c0d91";
    }}" # for TESO
    (builtins.toFile "line_jump" "\n") #TODO: I shouldn't need to manually add a line jump
    "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
  ];

  programs.gamemode.enable = true;

  environment.systemPackages = with pkgs; [
    # various tool
    #TODO:tmp
    pkgs.unrar
    pkgs.baobab
    pkgs.gparted
    pkgs.killall
    pkgs.zopfli
    pkgs.file
    pkgs.xorg.xinit
    pkgs.boinc
    pkgs.p7zip
    pkgs.gnumake
    pkgs.unzip
    pkgs.patchelf
    pkgs.bat
    pkgs.blueman
    # programming
    python-with-my-package
    cpufrequtils
    insomnia
    gtk4

    # KDE Apps
    ark
    gwenview
    konversation
    okular
    oxygen
    oxygen-icons5
    oxygenfonts
    redshift-plasma-applet
    spectacle
    libsForQt5.plasma-vault
    dolphin
    krita

    keepassxc

    #libreoffice #problem with CA
    xorg.libXi
    xorg.libX11
    xorg.libXcursor
    xorg.libXrandr # for some rust library that invoke a GUI

    (keepass.override { plugins = [ keepass-otpkeyprov ]; })
    libsForQt5.kwin-tiling
    kde-rounded-corners
    wacomtablet
    osu-lazer
    gst_all_1.gstreamer.out
  ];

  nixpkgs.config = {
    allowUnfree = true;
    calibre.unrarSupport = true;
  };

  fonts.fonts = with pkgs; [
    fira-code
  ];
  
  # for nixos-container
  networking.networkmanager.unmanaged = [ "interface-name:ve-*" ];
  networking.nat.enable = true;
  networking.nat.internalInterfaces = [ "ve-+" ];
  networking.nat.externalInterface = "enp2s0";


  hardware = {
    cpu.intel.updateMicrocode = true;
    cpu.amd.updateMicrocode = true;
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
      setLdLibraryPath = true;
    };
    bluetooth = {
      enable = true; # for dolphin-emu
    };
  };


  # Use the systemd-boot EFI boot loader.
  boot = {
    loader = {
      systemd-boot.enable = false;
      efi.canTouchEfiVariables = true;
      grub.useOSProber = true;
      grub.enable = true;
      grub.efiSupport = true;
    };
    supportedFilesystems = [ "btrfs" "vfat" "ntfs" ];
  };
  
  networking.networkmanager.enable = true;

  #TODO: firacode ?
  console.font = "Lat2-Terminus16";
  console.keyMap = "fr";

  i18n = {
    supportedLocales = [ "fr_FR.UTF-8/UTF-8" "en_US.UTF-8/UTF-8" ];
    defaultLocale = "fr_FR.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Paris";

  # shell
  programs.fish = {
    enable = true;
  };

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    #support32Bit = true;
    package = pkgs.pulseaudioFull;
  };


  # Enable the X11 windowing system.
  services = {
    printing.enable = true;
    xserver = {
      enable = true;
      layout = "fr";
      xkbOptions = "eurosign:e";
      libinput.enable = true;
      displayManager.sddm.enable = true;
      desktopManager.plasma5.enable = true;
    };
    syncthing = {
      enable = false;
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.

  users = {
    users = {
      marius = {
        isNormalUser = true;
        description = "Marius. DAVID";
        extraGroups = [ "wheel" "bluetooth" "systemd-journal" "input" "docker" "vboxusers" "networkmanager" "wireshark" "libvirtd" ];
        uid = 1001;
        shell = pkgs.fish;
      };
    };
  };


  nix = {
    settings = {
      max-jobs = 8;
      sandbox = true;
      auto-optimise-store = true;
      trusted-users = [ "root" "marius" ];
    };
    distributedBuilds = true;
  };

  nixpkgs.config.allowBroken = true;
  programs.adb.enable = true;
  services.blueman.enable = true;

  services.tor.enable = true;
  services.tor.client.enable = true;
  services.tor.torsocks.enable = true;

  #0ad, kdeconnect
  networking.firewall = {
    allowedTCPPorts = [
      2000
      25565
      25564
      64738
      # lost colony
      843
      4520
      # factorio
      34197
      # snapserver
      1780
      1705
      1704
    ];
    allowedTCPPortRanges = [
      # lost colony
      {
        from = 4530;
        to = 4532;
      }
    ];
    allowedUDPPortRanges = [
      # lost colony
      {
        from = 5055;
        to = 5100;
      }
    ];
    allowedUDPPorts = [ 20595 25565 64738 25564 34197 ];
    extraCommands = ''
      iptables -I INPUT -p tcp --dport 1714:1764 -j ACCEPT
      iptables -I INPUT -p udp --dport 1714:1764 -j ACCEPT
    '';
  };

  services.udev.extraRules = ''
    ACTION=="add", ATTR{idVendor}=="03c3", RUN+="${pkgs.bash}/bin/sh -c '${pkgs.coreutils}/bin/echo 200 >/sys/module/usbcore/parameters/usbfs_memory_mb'"
    # All ASI Cameras and filter wheels
    SUBSYSTEMS=="usb", ATTR{idVendor}=="03c3", MODE="0666"
    # ZWO FOCUSER
    ACTION=="add", ATTRS{idVendor}=="03c3", ATTRS{idProduct}=="1f10", GROUP="users", MODE="0666"
    # FILTER WHEELS
    ACTION=="add", ATTRS{idVendor}=="03c3", ATTRS{idProduct}=="1f01", GROUP="users", MODE="0666"
    # arduino programmer
    SUBSYSTEM=="usb", SYSFS{idVendor}=="0403", SYSFS{idProduct}=="6001", ACTION=="add", GROUP="users", MODE="0666"
  '';

  /*services.tt-rss = {
    enable = true;
    selfUrlPath = "http://localhost:89";
    virtualHost = "tt-rss";
    singleUserMode = true;
  };

  services.nginx.virtualHosts.tt-rss = {
    listen = [
      {
        addr = "localhost";
        port = 89;
        ssl = false;
      }
    ];
  };

  services.phpfpm.pools.tt-rss.phpPackage = pkgs.php.withExtensions (
    { enabled, all }: with all; [
      openssl
      mbstring
      simplexml
      curl
      json
      filter

      pdo
      pdo_pgsql
      pgsql
      pdo_mysql
      dom
      fileinfo
      intl
      session
    ]
  );

  nixpkgs.overlays = [
    (
      self: super: {
        rss-bridge = super.tt-rss.overrideAttrs (
          old: {
            prePatch = (old.prePatch or "") + ''
              substituteInPlace classes/urlhelper.php \
                --replace "80, 443" "80, 85, 443"
            '';
          }
        );
      }
    )
  ];*/


  services.redshift.enable = true;
  location.provider = "geoclue2";

  virtualisation.virtualbox.host = {
    enable = true;
    #enableExtensionPack = true;
  };

  virtualisation.docker.enable = true;


  networking.hosts = {
    "::1" = [ "example" ];
  };

  services.xserver.wacom.enable = true;

  # just in case DNS is broken
  #networking.nameservers = [ "1.1.1.1" "8.8.8.8" ];
  #services.bind.enable = true;

  /*services.openldap = {
    enable = true;
    urlList = [ "ldap://" "ldapi://" ];
    settings = {
      children = {
        "cn=schema".includes = [
          "${pkgs.openldap}/etc/schema/core.ldif"
          "${pkgs.openldap}/etc/schema/cosine.ldif"
          "${pkgs.openldap}/etc/schema/inetorgperson.ldif"
          "${pkgs.openldap}/etc/schema/nis.ldif"
        ];
        "olcDatabase={1}mdb" = {
          attrs = {
            objectClass = [ "olcDatabaseConfig" "olcMdbConfig" ];
            olcDatabase = "{1}mdb";
            olcDbDirectory = "/var/db/openldap";
            olcSuffix = "dc=example";
            olcRootDN = "cn=root,dc=example";
            olcRootPW = "{SSHA}DzSCMfFWfa44cOtOG5gtz1hKSkOLv5P8"; #pwsio
          };
        };
      };
    };
  };*/

  /*services.postgresql = {
    enable = true;
    #extraPlugins = with pkgs.postgresql_11.pkgs; [ postgis ];
    package = pkgs.postgresql_14;
  };*/


  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "20.09"; # Did you read the comment?
}
