{ config, pkgs, niximport, ... }:

with builtins;
with pkgs.lib;

# my games
let
  zeroadConfPath = ".config/0ad";
  ddaPath = ".cataclysm-dda";
  mariusnur = import <mariusnur> { inherit pkgs; };

  all_minecraft = import ./all-minecrafts.nix { inherit pkgs; };

  recastai = pkgs.stdenv.mkDerivation {
    pname = "recastai";
    version = "openmw";
    
    src = pkgs.fetchFromGitHub {
      owner = "recastnavigation";
      repo = "recastnavigation";
      rev = "e75adf86f91eb3082220085e42dda62679f9a3ea";
      sha256 = "sha256-HvVpxUDSy7KfiHJ/uOjgAVSGT0gxlPt5gagbxqhZcZo=";
    };

    nativeBuildInputs = [ pkgs.cmake ];

    buildInputs = [ pkgs.libGL pkgs.SDL2 pkgs.libGLU ];
  };

  bullet_openmw = pkgs.bullet.overrideAttrs (old: {
    cmakeFlags = (old.cmakeFlags or []) ++ [ "-DUSE_DOUBLE_PRECISION=ON" "-DBULLET2_MULTITHREADING=ON"];
  });

  collada-dom = pkgs.stdenv.mkDerivation {
    pname = "collada-dom";
    version = "master";

    src = pkgs.fetchFromGitHub {
      owner = "rdiankov";
      repo = "collada-dom";
      rev = "c1e20b7d6ff806237030fe82f126cb86d661f063";
      sha256 = "A1ne/D6S0shwCzb9spd1MoSt/238HWA8dvgd+DC9cXc=";
    };

    /*src = pkgs.fetchurl {
      url = "mirror://sourceforge/collada-dom/Collada%20DOM/Collada%20DOM%202.2/Collada%20DOM%202.2.zip";
      name = "collada-dom-2.2.0.zip";
      sha256 = "sha256-Vr0gw5eHeA55Zko7X49hwAKAoXdS2PHzvGGR6oqg26Y=";
    };

    prePatch = ''
      cp Makefile.linux Makefile
      make os=linux project=minizip
    '';

    makeFlags = [
      "os=linux"
      "project=dom"
      "-C" "dom"
    ];*/

    postInstall = ''
      chmod +w -R $out
      mv $out/include/*/* $out/include
    '';

    nativeBuildInputs = [ pkgs.cmake ];

    buildInputs = [
      pkgs.boost
      pkgs.libxml2
      pkgs.minizip
      pkgs.readline
    ];
  };

  customMods = self: super: pkgs.lib.recursiveUpdate super {
    soundpack.CC = pkgs.cataclysmDDA.buildSoundPack {
      modName = "CC-Sounds";
      version = "2022-08-23";
      src = pkgs.fetchzip {
        url = "https://github.com/Fris0uman/CDDA-Soundpacks/releases/download/2022-08-23/CC-Sounds.zip";
        sha256 = "sha256-OU22gsszPjPmD12clklk7sbK0XFgH4qo2CvmMIQDkFY=";
      };
      modRoot = ".";
    };
  };
  modded-cdda = pkgs.cataclysm-dda-git.withMods (mods: with mods.extend customMods; [
      soundpack.CC
  ]);

in
{
  home.packages = [
    pkgs.freedroidrpg
    pkgs.superTux
    /* (pkgs.dwarf-fortress-packages.dwarf-fortress-full.override { enableLegendsBrowser = true; }) */
    #		pkgs.openmw
    /*(
      pkgs.openmw.overrideAttrs (
        old: {
          src = pkgs.fetchFromGitHub {
            owner = "OpenMW";
            repo = "openmw";
            rev = "e3d3b97da04ca1622b29be555e77716182e79272";
            sha256 = "sha256-JFEQEHfQ3UXpvndz3ZzZwGzZM2COapdPfvP4XJh1tos=";
          };
          name = "openmw-custom";
          buildInputs = (builtins.map (x: if (x.pname or "") == "bullet" then
            bullet_openmw
          else if (x.pname or "") == "openscenegraph" then
            pkgs.openscenegraph.overrideAttrs (old: {
              COLLADA_DIR = collada-dom;
              buildInputs = (old.buildInputs or []) ++ [ pkgs.boost ];

              src = pkgs.fetchFromGitHub {
                owner = "OpenMW";
                repo = "osg";
                rev = "9e3088ffdc45a1c61e9bd193bad5dac45db3bb17"; #3.6 branch
                sha256 = "sha256-r5acVOT7TY2Uj7uLiuYnpqsM2IfQg22EbiwwDYjBah4=";
              };
            })
          else
            x
          ) old.buildInputs) ++ [ pkgs.lz4 recastai ];
          postPatch = ''
            				substituteInPlace components/nif/niffile.cpp \
            					--replace "NIFFile::sLoadUnsupportedFiles = false;" "NIFFile::sLoadUnsupportedFiles = true;"
            			'';

          cmakeFlags = (old.cmakeFlags or []) ++ [ "-DOPENMW_USE_SYSTEM_RECASTNAVIGATION=1" ];
        }
      )
    )*/

    pkgs.endless-sky
    (
      pkgs.retroarch.override {
        cores = with pkgs.libretro; [
          pcsx-rearmed
          beetle-psx
          beetle-psx-hw
          mgba
          snes9x
          desmume
          gambatte
        ];
      }
    )
    #		all_minecraft.versions.v1_16_4.client
    #niximport.mlpnur.my-little-investigation
    niximport.mlpnur.my-little-karaoke-beta
    niximport.mlpnur.trotmania-full
    modded-cdda
  ];
}
