{ config, pkgs, lib, ... }:

let
	common = pkgs.callPackage ./root/common.nix {};

	style = common.ricenur.ricePkgs.glasscord_default_theme;

	defineTheme = common.ricenur.function.set_glasscord_theme_script common.ricenur.ricePkgs.glasscord_default_theme;
in {
	home.packages = [ pkgs.discord ];
	#home.activation.setGlasscordTheme = lib.hm.dag.entryAfter [ "writeBoundary" ] defineTheme;
#	home.packages = [ pkgs.discord ];
}
