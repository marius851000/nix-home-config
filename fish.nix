{ config, pkgs, niximport, ... }:

# fish shell data


with pkgs;
with lib;
with builtins;

let
  ponysay = pkgs.ponysay;
  pokemonsay = niximport.mariusnur.pokemonsay;

  #TODO: should be upstreamed
  cowsay = niximport.mariusnur.cowsay.override { packages = [ niximport.mariusnur.cowsay-files ]; };

  lolcat = pkgs.lolcat;
  starship = pkgs.starship;

  fortunefuturama = fetchzip {
    url = "https://www.netmeister.org/apps/fortune-mod-futurama-0.2.tar.gz";
    sha256 = "0b9zs0r2jml3cqiv5wcinhv1wmcgzk2r07wy298g08ya94x9xan6";
  };
  fortune = pkgs.fortune;
in
{
  programs.fish = {
    enable = true;

    interactiveShellInit = ''
      			set number (random 0 10)
      			if test $number -ge 8
      				${ponysay}/bin/ponysay --ponyonly --anypony
      			else if test $number -ge 5
      				${ponysay}/bin/ponysay -q
      			else if test $number -ge 3
      				${fortune}/bin/fortune | ${pokemonsay}/bin/pokemonsay
      			else if test $number -ge 1
      				${cowsay}/bin/cowsay -f (ls ${cowsay}/share/cows | shuf -n1) (${fortune}/bin/fortune)
      			else
      				${cowsay}/bin/cowsay -f zoidberg (${fortune}/bin/fortune ${fortunefuturama}/futurama)
       			end
                              source ("${starship}/bin/starship" init fish --print-full-init | psub)
      		'';

    shellAliases = {
      l = "ls -lAh";
      lsi = "l --sort=size";

    };

    /*		promptInit = ''
    		function fish_prompt
    			set stat $status

    			echo -n "["(date +%R)]" "
    		  echo -n "$PWD " | ${lolcat}/bin/lolcat -ft -F 0.3

    			#if set -l git_branch (git show-branch --list 2> /dev/null| string split "]" | head -n 1 | string replace "* [" "" 2> /dev/null) #TODO: a better detectop, of the branch
    			#	set -l git_uncomitted (git status --porcelain | wc -l)
    			#	if test $git_uncomitted -ne 0
    			#	 	set_color red
    			#	else
    			#		set_color green
    			#	end
    			#	echo -n git:$git_branch
    			#	if test $git_uncomitted -ne 0
    			#		echo -n "/"$git_uncomitted
    			#	end
    			#	echo -n " "
    			#	set_color normal
    			#end
    			#TODO: optimise
    			switch "$stat"
    				case 0
    				case "*"
    					set_color -o red
    					echo -n "($stat) "
    					set_color normal
    			end
    			echo
    		  echo -n '> '
    			#TODO: include clementine
    		end
    		'';*/
  };

  home.packages = [
    lolcat
    pokemonsay
    cowsay
    ponysay
    fortune
  ];

}
