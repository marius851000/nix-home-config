{ pkgs, niximport, ... }:

#TODO: upstream

let
  rescrobbled = pkgs.rustPlatform.buildRustPackage rec {
    pname = "rescrobbled";
    version = "0.6.2";

    src = pkgs.fetchFromGitHub {
      owner = "InputUsername";
      repo = pname;
      rev = "v${version}";
      sha256 = "sha256-g6YGqphXB3dI3gnxw7MtTs1c5mIf6CFdfg2KjEVJhDU=";
    };

    cargoSha256 = "sha256-pbicRLYEHOlt0e4gtEEfc/b03Mg9m+pR9deMj/k42OQ=";

    buildInputs = [ pkgs.openssl.dev pkgs.dbus ];

    nativeBuildInputs = [ pkgs.pkg-config ];

    doCheck = false;
  };
in
{
  home.packages = [ rescrobbled ];

  systemd.user.services.rescrobbled = {
    Unit = {
      Description = "An MPRIS scrobbler";
    };

    Service = {
      Type = "simple";
      ExecStart = "${rescrobbled}/bin/rescrobbled";
    };

    Install = {
      WantedBy = [ "default.target" ];
    };
  };

  home.file.".config/rescrobbled/config.toml".text = ''
    listenbrainz-token = "${niximport.private.listenbrainz.token}"
    player-whitelist = [ "amarok", "elisa" ]
    filter-script = "${pkgs.writeScript "filter.py" ''
      #!${pkgs.python3}/bin/python3

      import sys

      artist, title, album, idk = (l.rstrip() for l in sys.stdin.readlines())

      # Ignore all tracks by specific artists

      if album == "Radio Brony":
        album = ""
      if artist == "https://radiobrony.fr":
        pass
      else:
        print(artist, title, album, sep='\n')
  ''}"
  '';
}
