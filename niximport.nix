{ pkgs, mariusnur_src, mlpnur_src, ricenur_src, funkinnur_flake, profile, plymouth-nixos-pony-src, ... }:

rec {
  inherit profile;
  mariusnur = import mariusnur_src { inherit pkgs; };
  images = import ./imagemix.nix {
    inherit pkgs mariusnur;
  };
  mlpnur = import mlpnur_src { inherit pkgs; };
  ricenur = import ricenur_src { inherit pkgs; };
  funkinnur = import funkinnur_flake.packages."${pkgs.stdenv.system}";
  profileutil = import ./profileutil.nix;
  plymouth-nixos-pony = import plymouth-nixos-pony-src { inherit pkgs; };
  private = if (profileutil.get_or_default profile "use_private" false) then import ./private/private.nix { } else { };
}