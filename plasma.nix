{ config, pkgs, niximport, ... }:

with builtins;
# plasma related setting

let
	kvantumDir = ".config/Kvantum";
	customarcdark = niximport.mariusnur.arcdarkcustom;


	#TOOD: check the mariusnur and arcdarkcustom stuff
	sweet_kvantum_theme = pkgs.stdenv.mkDerivation {
		name = "sweet_kvantum_theme";

		src = pkgs.fetchFromGitHub {
			owner = "EliverLara";
			repo = "Sweet";
			rev = "ef205d53e6e8409631a8c3c6604313bc642b41f6";
			sha256 = "EXTz268tmtBcO47oxshOU9Vj3+JfSzbq4twJOMm5iH4=";
		};

		patchPhase = ''
			substituteInPlace kde/kvantum/Sweet-transparent-toolbar.kvconfig \
				--replace "opaque=" "opaque=yuzu,yuzu-ea,"
		'';

		installPhase = ''
			mkdir $out
			cp kde/kvantum/Sweet-transparent-toolbar.kvconfig $out
			cp kde/kvantum/Sweet.svg $out/Sweet-transparent-toolbar.svg
		'';
	};

	plasma_config = let
		plasmarc = pkgs.writeText "plasmarc" (pkgs.lib.generators.toINI {} {
			Theme = {
				name = "breeze-dark";
			};
		});
	in
		pkgs.stdenv.mkDerivation {
		name = "plasma-marius-config";
		dontUnpack = true;

		installPhase = ''
			mkdir -p $out/etc/xdg
			ln -s ${plasmarc} $out/etc/xdg/plasmarc
		'';
	};
in
{
	home.packages = [
		pkgs.papirus-icon-theme
		pkgs.libsForQt5.qtstyleplugin-kvantum
		#customarcdark
	];

	home.file = {
		"${kvantumDir}/Sweet-transparent-toolbar".source = sweet_kvantum_theme;
		"${kvantumDir}/kvantum.kvconfig".text = ''
			[General]
			theme=Sweet-transparent-toolbar
			opaque=QMPlay2,kaffeine,kmplayer,subtitlecomposer,kdenlive,vlc,avidemux,avidemux2_qt4,avidemux3_qt4,avidemux3_qt5,kamoso,QtCreator,VirtualBox,trojita,dragon,digikam,virtualboxvm,yuzu,yuzu-ea
		'';
	};

	#services.kdeconnect.enable = true;

	xdg.systemDirs.config = [ "${plasma_config}/etc/xdg" ];
}
